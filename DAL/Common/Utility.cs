﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Common
{
    public class Utility
    {
        //select cast(current_value as int) from sys.sequences where name = 'AccCode'
        public int autoNextNumber(string type)
        {
            string q = "";
            switch (type)
            {
                case "InvoiceNo":
                    //q = "SELECT NEXT VALUE FOR dbo.InvNo";
                    q = "select cast(current_value as int) from sys.sequences where name = 'InvNo'";
                    break;
                case "VoucherNo":
                    //q = "SELECT NEXT VALUE FOR dbo.VoucherNo";
                    q = "select cast(current_value as int) from sys.sequences where name = 'VoucherNo'";
                    break;
                case "AccCode":
                    //q = "SELECT NEXT VALUE FOR dbo.AccCode";
                    q = "select cast(current_value as int) from sys.sequences where name = 'AccCode'";
                    break;
                case "ItemCode":
                    //q = "SELECT NEXT VALUE FOR dbo.ItemCode";
                    q = "select cast(current_value as int) from sys.sequences where name = 'ItemCode'";
                    break;
            }
            AdoHelper db = new AdoHelper();
            int res = Convert.ToInt32(db.ExecScalar(q));
            return res;
        }
    }
}
