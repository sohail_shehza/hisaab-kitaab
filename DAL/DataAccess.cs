﻿using DAL.AccountsDAL;
using DAL.Common;
using DAL.ProductsDAL;
using DAL.TransactionDAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DataAccess
    {
        DataAccess()
        {
            
            //string path = Directory.GetCurrentDirectory() + "License.txt";
            //AdoHelper.ConnectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\ERP.mdf;Integrated Security=True;User Instance=True;";
            AdoHelper.ConnectionString = "Server=DESKTOP-JUMLUSJ\\SQLEXPRESS01;Initial Catalog=HisaabKitaab;User Id=sa;password=admin";
            //AdoHelper.ConnectionString = "Server=DESKTOP-12DT46V\\SQLEXPRESS;Initial Catalog=HisaabKitaab;User Id=sa;password=admin";
            //ConfigMan configMan = new ConfigMan(this.GetType().Assembly.Location);
            // AdoHelper.ConnectionString = configMan.GetAppSetting("Connection_String");
        }
        public static DataAccess instance
        {
            get
            {
                return new DataAccess();
            }
        }
        public Accounts Accounts
        {
            get
            {
                return new Accounts();
            }
        }
        public AccountTypes AccountTypes
        {
            get
            {
                return new AccountTypes();
            }
        }
        public BadDebts BadDebts
        {
            get
            {
                return new BadDebts();
            }
        }
        public AccountCategories AccountCategories
        {
            get
            {
                return new AccountCategories();
            }
        }

        public Products Products
        {
            get
            {
                return new Products();
            }
        }

        public ProductCategories ProductCategories
        {
            get
            {
                return new ProductCategories();
            }
        }

        public Suppliers Suppliers
        {
            get
            {
                return new Suppliers();
            }
        }

        public Customers Customers
        {
            get
            {
                return new Customers();
            }
        }

        public PurchaseInvoice PurchaseInvoice
        {
            get
            {
                return new PurchaseInvoice();
            }
        }
        public DAL.UserManagement.UserManagement UserManagement
        {
            get
            {
                return new DAL.UserManagement.UserManagement();
            }
        }

        public SaleInvoice SaleInvoice
        {
            get
            {
                return new SaleInvoice();
            }
        }
        public CustomerOfferTable CustomerOfferTable
        {
            get
            {
                return new CustomerOfferTable();
            }
        }

        public CashBook CashBook
        {
            get
            {
                return new CashBook();
            }
        }
        public Journal Journal
        {
            get
            {
                return new Journal();
            }
        }

        public Voucher Voucher
        {
            get
            {
                return new Voucher();
            }
        }

        public VoucherDetails VoucherDetails
        {
            get
            {
                return new VoucherDetails();
            }
        }
        public TrialBalance TrialBalance
        {
            get
            {
                return new TrialBalance();
            }
        }

        public Utility Utility
        {
            get
            {
                return new Utility();
            }
        }
    }
}
