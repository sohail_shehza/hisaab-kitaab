﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.AccountsDAL
{
    public class Suppliers
    {
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Suppliers", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(int AccountId,string Name,string Address,string Phone,string City,string Country)
        {

            DataSet ds = new DataSet();
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "AccountId",AccountId,
                    "Name",Name,
                    "Address",Address,
                    "Phone",Phone,
                    "City",City,
                    "Country",Country
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Suppliers", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
