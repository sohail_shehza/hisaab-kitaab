﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Accounts
    {
        public DataTable GetAllBankAccounts()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAllBankAccounts" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAccById(int AccId)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAccById", "AccId", AccId };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAccByCode(string AccCode)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAccByCode", "AccCode" , AccCode };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetCashBankAccounts()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getCashBankAccounts" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetSupplierAccounts()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getSupplierAccounts" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetCustomerAccounts()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getCustomerAccounts" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Accounts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(string accCode,string title,int typeId,int categoryId)
        {
           
            DataSet ds = new DataSet();
            try
            {
                object[] param = { 
                    "Action", "Insert",
                    "AccCode",accCode,
                    "Title", title,
                    "TypeId", typeId,
                    "CatId", categoryId
                };
                AdoHelper db = new AdoHelper();
                int result = Convert.ToInt32(db.ExecScalarProc("SP_Accounts", param));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public int updateAccount(int accId,string accCode,string title, int typeId, int categoryId)
        {

            DataSet ds = new DataSet();
            try
            {
                object[] param = {
                    "Action", "updateAccount",
                    "AccId",accId,
                    "AccCode",accCode,
                    "Title", title,
                    "TypeId", typeId,
                    "CatId", categoryId
                };
                AdoHelper db = new AdoHelper();
                int result = Convert.ToInt32(db.ExecScalarProc("SP_Accounts", param));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Boolean delete()
        {
            try
            {
               
            }
            catch (Exception ex) { }
            return true;
        }
    }
}
