﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.AccountsDAL
{
    public class BadDebts
    {
        public DataTable GetAllBadDebts()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_BadDebts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetCustomerId(int customerId)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getBadDebtsCustomerId", "customerId", customerId };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_BadDebts", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public Boolean updateBadDebts(int customerId,  int DueAmount)
        {
            try
            {
                object[] param = {
                    "Action", "updateBadDebts",
                    "CustomerId",customerId,
                    "AmountDue",DueAmount,
                    
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_BadDebts", param);
                if (result != null)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
