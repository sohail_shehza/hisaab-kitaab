﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.AccountsDAL
{
    public class AccountTypes
    {
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_AccountTypes",param);
            }
            catch (Exception ex) {
                throw ex;
            }
            return ds.Tables[0];
        }
    }
}
