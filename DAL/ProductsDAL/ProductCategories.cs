﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ProductsDAL
{
    public class ProductCategories
    {
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_ProductCategories", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public Boolean insert(string title, string desc)
        {
           
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "Title", title,
                    "Desc", desc
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_ProductCategories", param);
                if (result != null)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
            
        }
    }
}
