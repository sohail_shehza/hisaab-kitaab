﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ProductsDAL
{
    public class Products
    {
        public DataTable GetDetailed(int pageNo)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getDetailed", "PageOffset", pageNo };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAllProductByCodeAndName(int PageOffset, int MaxRecords, string code = null, string name = null)
        {
            DataSet ds = new DataSet();
            //PageOffset = PageOffset == 0 ? 1 : PageOffset;
            //MaxRecords = MaxRecords == 0 ? 10 : MaxRecords;
            PageOffset = PageOffset == 1 ? 0 : PageOffset;
            MaxRecords = MaxRecords == 0 ? 10 : MaxRecords;

            try
            {
                object[] param = { "Action", "getDetailedByNameAndCode", "PageOffset", PageOffset, "MaxRecords", MaxRecords, "Barcode", code, "Title", name };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAllItemName()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getItemName" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetCurrentStock()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "stockDetail" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetByItemCode(string code)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getByItemCode","Barcode",code };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetProductById(int ProductId)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getProductById","ProductId",ProductId };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        //
        public DataTable GetProductByCategoryId(int catId,int pageNo)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getProductByCategoryId", "CategoryId", catId, "PageOffset", pageNo };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Products", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public Boolean insert(string barcode,
                                string title,
                                int catId,
                                string unit, 
                                decimal purchasePrice,
                                decimal salePrice)
        {
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "Barcode",barcode,
                    "Title",title,
                    "CategoryId",catId,
                    "Unit",unit,
                    "PurchasePrice",purchasePrice,
                    "SalePrice",salePrice
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Products", param);
                if (result != null)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public Boolean updateProduct(int productId,string barcode,string title,int catId,string unit,decimal purchasePrice,decimal salePrice)
        {
            try
            {
                object[] param = {
                    "Action", "updateProduct",
                    "ProductId",productId,
                    "Barcode",barcode,
                    "Title",title,
                    "CategoryId",catId,
                    "Unit",unit,
                    "PurchasePrice",purchasePrice,
                    "SalePrice",salePrice
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Products", param);
                if (result != null)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
