﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UserManagement
{
   public class UserManagement
    {
        public DataTable GetAll(string UserName, string Password)
        {
            DataSet ds = new DataSet();
          
            try
            {
                object[] param = { "Action", "getUser", "UserName", UserName, "Password", Password };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_UserManagement", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable ChangePassword(string UserName, string Password,string NewPassword)
        {
            DataSet ds = new DataSet();

            try
            {
                object[] param = { "Action", "changePassword", "UserName", UserName, "Password", Password, "NewPassword", NewPassword };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_UserManagement", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
    }
}
