﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TransactionDAL
{
    public class SaleInvoice
    {
        public DataTable GetInvById(int Id)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getInvoiceById","InvoiceId",Id };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_SaleInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetMaxInvoiceNo()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getMaxInvoiceNo" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_SaleInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAll(int PageOffset, int MaxRecords,string invoiceNo = null)
        {
            DataSet ds = new DataSet();
            //PageOffset = PageOffset == 0 ? 1 : PageOffset;
            //MaxRecords = MaxRecords == 0 ? 10 : MaxRecords;
            PageOffset = PageOffset == 1 ? 0 : PageOffset;
            MaxRecords = MaxRecords == 0 ? 10 : MaxRecords;

            try
            {
                object[] param = { "Action", "getAll", "PageOffset", PageOffset, "MaxRecords", MaxRecords, "InvoiceNo", invoiceNo };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_SaleInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        
        public int insert(string InvoiceNo, int CustomerId, DateTime Date, string Description,string FinalDiscount,decimal SubTotal,decimal Total,string MobileNo,decimal amountDue, decimal balanceDue)
        {
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "InvoiceNo",InvoiceNo,
                    "CustomerId",CustomerId,
                    "Date",Date,
                    "Description",Description,
                    "FinalDiscount",FinalDiscount,
                    "SubTotal",SubTotal,
                    "Total",Total,
                    "MobileNo",MobileNo,
                    "AmountDue",amountDue,
                    "BalanceDue",balanceDue
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_SaleInvoice", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public int InsertIntoBadDebits(int CustomerId, DateTime Date, decimal Total, string MobileNo, decimal amountDue)
        {
            try
            {
                object[] param = {
                    "Action", "InsertIntoBadDebits",
                    "CustomerId",CustomerId,
                     "MobileNo",MobileNo,
                     "Total",Total,
                     "AmountDue",amountDue,
                     "Date",Date,
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_SaleInvoice", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public void insertDetails(int SaleInvoiceId, int ProductId, decimal PurchasePrice, int Quantitiy,decimal SalePrice)
        {
            try
            {
                object[] param = {
                    "Action", "InsertDetails",
                    "SaleInvoiceId",SaleInvoiceId,
                    "ProductId",ProductId,
                    "PurchasePrice",PurchasePrice,
                    "Quantitiy",Quantitiy,
                    "SalePrice",SalePrice
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_SaleInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public DataTable GetReportInvByItemBarcode(int Id)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getByItemCode", "Barcode", Id };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_POSReporting", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetReportInvByItemDate(DateTime start,DateTime end)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getByItemDate", "startDate", start, "endDate", end };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_POSReporting", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
    }
}
