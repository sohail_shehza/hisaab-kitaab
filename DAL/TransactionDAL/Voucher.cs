﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TransactionDAL
{
    public class Voucher
    {
        public DataTable GetVoucherTypes()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getVoucherTypes" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Voucher", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Journal", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(int VoucherNo,int VoucherTypeID,int InvoiceID, DateTime Date, string Description)
        {
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "VoucherNo",VoucherNo,
                    "VoucherTypeID",VoucherTypeID,
                    "InvoiceID",InvoiceID,
                    "Date",Date,
                    "Description",Description
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Voucher", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int insertDetails(int VoucherId, int AccountId, decimal Debit, decimal Credit, string Remarks)
        {
            try
            {
                object[] param = {
                    "Action", "InsertDetails",
                    "VoucherId",VoucherId,
                    "AccountId",AccountId,
                    "Debit",Debit,
                    "Credit",Credit,
                    "Remarks",Remarks

                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Voucher", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
