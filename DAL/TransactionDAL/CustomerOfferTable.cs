﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TransactionDAL
{
   public class CustomerOfferTable
    {
        public DataTable GetByPhoneNo(string mobileNo)
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getByPhoneNo", "MobileNo", mobileNo };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_CustomerOfferTable", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(string mobileNo)
        {
            try
            {
                object[] param = { "Action", "getByPhoneNo", "MobileNo", mobileNo };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_CustomerOfferTable", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
