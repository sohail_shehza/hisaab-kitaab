﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Common;
namespace DAL.TransactionDAL
{
    public class TrialBalance
    {
        public DataTable Get()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "Get" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_TrialBalance", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
    }
}
