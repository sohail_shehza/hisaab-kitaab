﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TransactionDAL
{
    public class Journal
    {
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_Journal", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(int VoucherNo, DateTime Date, string Description)
        {
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "VoucherNo",VoucherNo,
                    "Date",Date,
                    "Description",Description
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Journal", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int insertDetails(int JournalId, int AccountId, decimal Debit, decimal Credit)
        {
            try
            {
                object[] param = {
                    "Action", "InsertDetails",
                    "JournalId",JournalId,
                    "AccountId",AccountId,
                    "Debit",Debit,
                    "Credit",Credit

                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_Journal", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
