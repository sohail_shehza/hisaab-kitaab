﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TransactionDAL
{
    public class CashBook
    {
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_CashBook", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(int VoucherNo, int CashBankId,DateTime Date,string Description,int AccountId,decimal Debit,decimal Credit,string Type,string CashDiscount)
        {
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "VoucherNo",VoucherNo,
                    "CashBankId",CashBankId,
                    "Date",Date,
                    "Description",Description,
                    "AccountId",AccountId,
                    "Debit",Debit,
                    "Credit",Credit,
                    "Type",Type,
                    "CashDiscount",CashDiscount
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_CashBook", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

    }
}
