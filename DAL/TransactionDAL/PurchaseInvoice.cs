﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TransactionDAL
{
    public class PurchaseInvoice
    {
        public DataTable GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getAll" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_PurchaseInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetMaxInvoiceNo()
        {
            DataSet ds = new DataSet();
            try
            {
                object[] param = { "Action", "getMaxInvoiceNo" };
                AdoHelper db = new AdoHelper();
                ds = db.ExecDataSetProc("SP_PurchaseInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int insert(string InvoiceNo,int SupplierId,DateTime Date,string Description,decimal SubTotal,decimal Discount,decimal Total)
        {
            try
            {
                object[] param = {
                    "Action", "Insert",
                    "InvoiceNo",InvoiceNo,
                    "SupplierId",SupplierId,
                    "Date",Date,
                    "Description",Description,
                    "SubTotal",SubTotal,
                    "Discount",Discount,
                    "Total",Total
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_PurchaseInvoice", param);
                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public void insertDetails(int PurchaseInvoiceId, int ProductId, decimal PurchasePrice, int Quantitiy)
        {
            try
            {
                object[] param = {
                    "Action", "InsertDetails",
                    "PurchaseInvoiceId",PurchaseInvoiceId,
                    "ProductId",ProductId,
                    "PurchasePrice",PurchasePrice,
                    "Quantitiy",Quantitiy
                };
                AdoHelper db = new AdoHelper();
                var result = db.ExecScalarProc("SP_PurchaseInvoice", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
