﻿namespace EXYPOS
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMainAccounts = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAccounts = new System.Windows.Forms.ToolStripMenuItem();
            this.vouchersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCreateVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.accountReceiveableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAddProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.menuProductsList = new System.Windows.Forms.ToolStripMenuItem();
            this.productCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suppliersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCreatePurchaseInvoice = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPurchaseInvoiceList = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRefreshBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.loaderPicBox = new System.Windows.Forms.PictureBox();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.recentOrderGrid = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.saleInvRepoView = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tBtnSale = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tBtnPurchase = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnManageProducts = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnManageBarcode = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tBtnExit = new System.Windows.Forms.ToolStripButton();
            this.btnNext = new System.Windows.Forms.Button();
            this.Prev = new System.Windows.Forms.Button();
            this.txtPageSize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SaleInvoiceSmall1 = new EXYPOS.Reports.SaleInvoiceSmall();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentOrderGrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.menuMainAccounts,
            this.administrationToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.adminToolStripMenuItem,
            this.menuRefreshBtn,
            this.helpToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1010, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // menuMainAccounts
            // 
            this.menuMainAccounts.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAccounts,
            this.vouchersToolStripMenuItem,
            this.accountReceiveableToolStripMenuItem});
            this.menuMainAccounts.Name = "menuMainAccounts";
            this.menuMainAccounts.Size = new System.Drawing.Size(69, 20);
            this.menuMainAccounts.Text = "Accounts";
            this.menuMainAccounts.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // menuAccounts
            // 
            this.menuAccounts.Name = "menuAccounts";
            this.menuAccounts.Size = new System.Drawing.Size(184, 22);
            this.menuAccounts.Text = "Chart of Accounts";
            this.menuAccounts.Click += new System.EventHandler(this.menuAccounts_Click);
            // 
            // vouchersToolStripMenuItem
            // 
            this.vouchersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCreateVoucher});
            this.vouchersToolStripMenuItem.Name = "vouchersToolStripMenuItem";
            this.vouchersToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.vouchersToolStripMenuItem.Text = "Vouchers";
            // 
            // menuCreateVoucher
            // 
            this.menuCreateVoucher.Name = "menuCreateVoucher";
            this.menuCreateVoucher.Size = new System.Drawing.Size(154, 22);
            this.menuCreateVoucher.Text = "Create Voucher";
            this.menuCreateVoucher.Click += new System.EventHandler(this.menuCreateVoucher_Click);
            // 
            // accountReceiveableToolStripMenuItem
            // 
            this.accountReceiveableToolStripMenuItem.Name = "accountReceiveableToolStripMenuItem";
            this.accountReceiveableToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.accountReceiveableToolStripMenuItem.Text = "Account Receiveable";
            this.accountReceiveableToolStripMenuItem.Click += new System.EventHandler(this.accountReceiveableToolStripMenuItem_Click);
            // 
            // administrationToolStripMenuItem
            // 
            this.administrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productsToolStripMenuItem,
            this.suppliersToolStripMenuItem,
            this.customersToolStripMenuItem,
            this.employeesToolStripMenuItem,
            this.purchaseInvoiceToolStripMenuItem});
            this.administrationToolStripMenuItem.Name = "administrationToolStripMenuItem";
            this.administrationToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.administrationToolStripMenuItem.Text = "Administration";
            this.administrationToolStripMenuItem.Click += new System.EventHandler(this.administrationToolStripMenuItem_Click);
            // 
            // productsToolStripMenuItem
            // 
            this.productsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddProduct,
            this.menuProductsList,
            this.productCategoryToolStripMenuItem});
            this.productsToolStripMenuItem.Name = "productsToolStripMenuItem";
            this.productsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.productsToolStripMenuItem.Text = "Products";
            // 
            // menuAddProduct
            // 
            this.menuAddProduct.Name = "menuAddProduct";
            this.menuAddProduct.Size = new System.Drawing.Size(167, 22);
            this.menuAddProduct.Text = "Add Product";
            this.menuAddProduct.Click += new System.EventHandler(this.menuAddProduct_Click);
            // 
            // menuProductsList
            // 
            this.menuProductsList.Name = "menuProductsList";
            this.menuProductsList.Size = new System.Drawing.Size(167, 22);
            this.menuProductsList.Text = "Products List";
            this.menuProductsList.Click += new System.EventHandler(this.menuProductsList_Click);
            // 
            // productCategoryToolStripMenuItem
            // 
            this.productCategoryToolStripMenuItem.Name = "productCategoryToolStripMenuItem";
            this.productCategoryToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.productCategoryToolStripMenuItem.Text = "Product Category";
            this.productCategoryToolStripMenuItem.Click += new System.EventHandler(this.productCategoryToolStripMenuItem_Click);
            // 
            // suppliersToolStripMenuItem
            // 
            this.suppliersToolStripMenuItem.Name = "suppliersToolStripMenuItem";
            this.suppliersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.suppliersToolStripMenuItem.Text = "Suppliers";
            this.suppliersToolStripMenuItem.Click += new System.EventHandler(this.suppliersToolStripMenuItem_Click);
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.customersToolStripMenuItem.Text = "Customers";
            this.customersToolStripMenuItem.Click += new System.EventHandler(this.customersToolStripMenuItem_Click);
            // 
            // employeesToolStripMenuItem
            // 
            this.employeesToolStripMenuItem.Name = "employeesToolStripMenuItem";
            this.employeesToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.employeesToolStripMenuItem.Text = "Employees";
            // 
            // purchaseInvoiceToolStripMenuItem
            // 
            this.purchaseInvoiceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCreatePurchaseInvoice,
            this.menuPurchaseInvoiceList});
            this.purchaseInvoiceToolStripMenuItem.Name = "purchaseInvoiceToolStripMenuItem";
            this.purchaseInvoiceToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.purchaseInvoiceToolStripMenuItem.Text = "Purchase Invoice";
            // 
            // menuCreatePurchaseInvoice
            // 
            this.menuCreatePurchaseInvoice.Name = "menuCreatePurchaseInvoice";
            this.menuCreatePurchaseInvoice.Size = new System.Drawing.Size(200, 22);
            this.menuCreatePurchaseInvoice.Text = "Create Purchase Invoice";
            this.menuCreatePurchaseInvoice.Click += new System.EventHandler(this.menuCreatePurchaseInvoice_Click);
            // 
            // menuPurchaseInvoiceList
            // 
            this.menuPurchaseInvoiceList.Name = "menuPurchaseInvoiceList";
            this.menuPurchaseInvoiceList.Size = new System.Drawing.Size(200, 22);
            this.menuPurchaseInvoiceList.Text = "Purchase Invoice List";
            this.menuPurchaseInvoiceList.Click += new System.EventHandler(this.menuPurchaseInvoiceList_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            this.reportsToolStripMenuItem.Click += new System.EventHandler(this.reportsToolStripMenuItem_Click);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "Admin";
            this.adminToolStripMenuItem.Click += new System.EventHandler(this.adminToolStripMenuItem_Click);
            // 
            // menuRefreshBtn
            // 
            this.menuRefreshBtn.Name = "menuRefreshBtn";
            this.menuRefreshBtn.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.menuRefreshBtn.Size = new System.Drawing.Size(58, 20);
            this.menuRefreshBtn.Text = "Refresh";
            this.menuRefreshBtn.Click += new System.EventHandler(this.menuRefreshBtn_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 594);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1010, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(160, 17);
            this.toolStripStatusLabel1.Text = "Created by (+92-3344778666)";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.loaderPicBox);
            this.panel1.Controls.Add(this.txtInvoiceNo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(992, 65);
            this.panel1.TabIndex = 4;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // loaderPicBox
            // 
            this.loaderPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loaderPicBox.Image = ((System.Drawing.Image)(resources.GetObject("loaderPicBox.Image")));
            this.loaderPicBox.Location = new System.Drawing.Point(905, 3);
            this.loaderPicBox.Name = "loaderPicBox";
            this.loaderPicBox.Size = new System.Drawing.Size(80, 58);
            this.loaderPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loaderPicBox.TabIndex = 1;
            this.loaderPicBox.TabStop = false;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Location = new System.Drawing.Point(73, 16);
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(100, 20);
            this.txtInvoiceNo.TabIndex = 1;
            this.txtInvoiceNo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "lnvoice #";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.recentOrderGrid);
            this.groupBox1.Location = new System.Drawing.Point(12, 178);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(622, 303);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recent Orders";
            // 
            // recentOrderGrid
            // 
            this.recentOrderGrid.AllowUserToAddRows = false;
            this.recentOrderGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.recentOrderGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.recentOrderGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.recentOrderGrid.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.recentOrderGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.recentOrderGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.recentOrderGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.recentOrderGrid.EnableHeadersVisualStyles = false;
            this.recentOrderGrid.Location = new System.Drawing.Point(6, 19);
            this.recentOrderGrid.MultiSelect = false;
            this.recentOrderGrid.Name = "recentOrderGrid";
            this.recentOrderGrid.ReadOnly = true;
            this.recentOrderGrid.RowHeadersVisible = false;
            this.recentOrderGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.recentOrderGrid.Size = new System.Drawing.Size(606, 278);
            this.recentOrderGrid.TabIndex = 0;
            this.recentOrderGrid.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.recentOrderGrid_CellContentDoubleClick);
            this.recentOrderGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.recentOrderGrid_CellDoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.saleInvRepoView);
            this.groupBox2.Location = new System.Drawing.Point(640, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(364, 412);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Report Preview";
            // 
            // saleInvRepoView
            // 
            this.saleInvRepoView.ActiveViewIndex = -1;
            this.saleInvRepoView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.saleInvRepoView.Cursor = System.Windows.Forms.Cursors.Default;
            this.saleInvRepoView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saleInvRepoView.Location = new System.Drawing.Point(3, 16);
            this.saleInvRepoView.Name = "saleInvRepoView";
            this.saleInvRepoView.ShowCloseButton = false;
            this.saleInvRepoView.ShowGotoPageButton = false;
            this.saleInvRepoView.ShowGroupTreeButton = false;
            this.saleInvRepoView.ShowLogo = false;
            this.saleInvRepoView.ShowPageNavigateButtons = false;
            this.saleInvRepoView.ShowParameterPanelButton = false;
            this.saleInvRepoView.ShowRefreshButton = false;
            this.saleInvRepoView.ShowTextSearchButton = false;
            this.saleInvRepoView.Size = new System.Drawing.Size(358, 393);
            this.saleInvRepoView.TabIndex = 0;
            this.saleInvRepoView.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tBtnSale,
            this.toolStripSeparator1,
            this.tBtnPurchase,
            this.toolStripSeparator2,
            this.tbtnManageProducts,
            this.toolStripSeparator3,
            this.tbtnManageBarcode,
            this.toolStripSeparator4,
            this.toolStripButton5,
            this.toolStripSeparator5,
            this.toolStripButton6,
            this.toolStripSeparator6,
            this.toolStripButton7,
            this.toolStripSeparator7,
            this.toolStripButton8,
            this.toolStripSeparator8,
            this.toolStripButton9,
            this.toolStripSeparator9,
            this.tBtnExit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1010, 78);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tBtnSale
            // 
            this.tBtnSale.AutoSize = false;
            this.tBtnSale.Image = global::EXYPOS.Properties.Resources.cart;
            this.tBtnSale.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tBtnSale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tBtnSale.Name = "tBtnSale";
            this.tBtnSale.Size = new System.Drawing.Size(100, 60);
            this.tBtnSale.Text = "Sale and Return";
            this.tBtnSale.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tBtnSale.Click += new System.EventHandler(this.tBtnSale_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 78);
            // 
            // tBtnPurchase
            // 
            this.tBtnPurchase.AutoSize = false;
            this.tBtnPurchase.Image = global::EXYPOS.Properties.Resources.cash;
            this.tBtnPurchase.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tBtnPurchase.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tBtnPurchase.Name = "tBtnPurchase";
            this.tBtnPurchase.Size = new System.Drawing.Size(100, 75);
            this.tBtnPurchase.Text = "New Purchase";
            this.tBtnPurchase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tBtnPurchase.Click += new System.EventHandler(this.tBtnPurchase_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 78);
            // 
            // tbtnManageProducts
            // 
            this.tbtnManageProducts.AutoSize = false;
            this.tbtnManageProducts.Image = global::EXYPOS.Properties.Resources.ManageProducts;
            this.tbtnManageProducts.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnManageProducts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnManageProducts.Name = "tbtnManageProducts";
            this.tbtnManageProducts.Size = new System.Drawing.Size(100, 75);
            this.tbtnManageProducts.Text = "Manage Products";
            this.tbtnManageProducts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtnManageProducts.Click += new System.EventHandler(this.tbtnManageProducts_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 78);
            // 
            // tbtnManageBarcode
            // 
            this.tbtnManageBarcode.AutoSize = false;
            this.tbtnManageBarcode.Image = global::EXYPOS.Properties.Resources.barcode;
            this.tbtnManageBarcode.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnManageBarcode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnManageBarcode.Name = "tbtnManageBarcode";
            this.tbtnManageBarcode.Size = new System.Drawing.Size(100, 75);
            this.tbtnManageBarcode.Text = "Manage Barcode";
            this.tbtnManageBarcode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtnManageBarcode.Click += new System.EventHandler(this.tbtnManageBarcode_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 78);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.AutoSize = false;
            this.toolStripButton5.Image = global::EXYPOS.Properties.Resources.backup;
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(100, 75);
            this.toolStripButton5.Text = "Backup Database";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 78);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.AutoSize = false;
            this.toolStripButton6.Image = global::EXYPOS.Properties.Resources.restore;
            this.toolStripButton6.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(100, 75);
            this.toolStripButton6.Text = "Restore Database";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 78);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.AutoSize = false;
            this.toolStripButton7.Image = global::EXYPOS.Properties.Resources.key;
            this.toolStripButton7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(100, 75);
            this.toolStripButton7.Text = "Change Password";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 78);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.AutoSize = false;
            this.toolStripButton8.Image = global::EXYPOS.Properties.Resources.settings;
            this.toolStripButton8.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(100, 75);
            this.toolStripButton8.Text = "System Setup";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 78);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.AutoSize = false;
            this.toolStripButton9.Image = global::EXYPOS.Properties.Resources.notification;
            this.toolStripButton9.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(100, 75);
            this.toolStripButton9.Text = "Notifications";
            this.toolStripButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 78);
            // 
            // tBtnExit
            // 
            this.tBtnExit.AutoSize = false;
            this.tBtnExit.Image = global::EXYPOS.Properties.Resources.exit;
            this.tBtnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tBtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tBtnExit.Name = "tBtnExit";
            this.tBtnExit.Size = new System.Drawing.Size(75, 75);
            this.tBtnExit.Text = "Exit Program";
            this.tBtnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tBtnExit.Click += new System.EventHandler(this.tBtnExit_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnNext.Location = new System.Drawing.Point(567, 12);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(47, 25);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // Prev
            // 
            this.Prev.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Prev.Location = new System.Drawing.Point(412, 12);
            this.Prev.Name = "Prev";
            this.Prev.Size = new System.Drawing.Size(47, 25);
            this.Prev.TabIndex = 2;
            this.Prev.Text = "Prev";
            this.Prev.UseVisualStyleBackColor = true;
            this.Prev.Click += new System.EventHandler(this.Prev_Click);
            // 
            // txtPageSize
            // 
            this.txtPageSize.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtPageSize.Location = new System.Drawing.Point(471, 12);
            this.txtPageSize.Multiline = true;
            this.txtPageSize.Name = "txtPageSize";
            this.txtPageSize.ReadOnly = true;
            this.txtPageSize.Size = new System.Drawing.Size(52, 25);
            this.txtPageSize.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(527, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "of";
            // 
            // lblCount
            // 
            this.lblCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(545, 18);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(13, 13);
            this.lblCount.TabIndex = 5;
            this.lblCount.Text = "0";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox3.Controls.Add(this.btnNext);
            this.groupBox3.Controls.Add(this.lblCount);
            this.groupBox3.Controls.Add(this.Prev);
            this.groupBox3.Controls.Add(this.txtPageSize);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(12, 475);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(622, 46);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1010, 616);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Point of Sale";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentOrderGrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuMainAccounts;
        private System.Windows.Forms.ToolStripMenuItem administrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView recentOrderGrid;
        private System.Windows.Forms.TextBox txtInvoiceNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tBtnSale;
        private System.Windows.Forms.ToolStripButton tBtnPurchase;
        private System.Windows.Forms.ToolStripButton tbtnManageProducts;
        private System.Windows.Forms.ToolStripButton tbtnManageBarcode;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton tBtnExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem productsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuAddProduct;
        private System.Windows.Forms.ToolStripMenuItem menuProductsList;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem suppliersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuAccounts;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer saleInvRepoView;
        private Reports.SaleInvoiceSmall SaleInvoiceSmall1;
        private System.Windows.Forms.ToolStripMenuItem vouchersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCreateVoucher;
        private System.Windows.Forms.ToolStripMenuItem purchaseInvoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCreatePurchaseInvoice;
        private System.Windows.Forms.ToolStripMenuItem menuPurchaseInvoiceList;
        private System.Windows.Forms.ToolStripMenuItem menuRefreshBtn;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productCategoryToolStripMenuItem;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPageSize;
        private System.Windows.Forms.Button Prev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.PictureBox loaderPicBox;
        private System.Windows.Forms.ToolStripMenuItem accountReceiveableToolStripMenuItem;
    }
}

