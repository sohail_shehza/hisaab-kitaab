﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            HideLabels();
        }
        private void HideLabels()
        {
            lblUserName.Hide();
            lblPassword.Hide();
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            bool isValid = true;
            if(txtUserName.Text.Trim() == "")
            {
                isValid = false;
                lblUserName.Show();
            }
            if (txtUserPassword.Text.Trim() == "")
            {
                isValid = false;
                lblPassword.Show();
            }
            var dt = DataAccess.instance.UserManagement.GetAll(txtUserName.Text.Trim(), txtUserPassword.Text.Trim());
            if (dt.Rows.Count <= 0)
            {
                isValid = false;
                loaderPicBox.Hide();
            }
            if (isValid)
            {
                this.Visible = false;
              
                 MainForm form = new MainForm();
                form.Show(this);
               
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length <= 0)
            {
                lblUserName.Show();
              
            }
            else
            {
                lblUserName.Hide();
            }
           
        }

        private void txtUserPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length <= 0)
            {
                lblPassword.Show();
            }
            else
            {
                lblPassword.Hide();
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            loaderPicBox.Show();
        }
    }
}
