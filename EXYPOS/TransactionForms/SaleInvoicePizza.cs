﻿using DAL;
using EXYPOS.Model;
using EXYPOS.ReportForms;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.TransactionForms
{
    public partial class SaleInvoicePizza : Form
    {
        int totalPage = 0;
        decimal DueAmount = 0;
        decimal BalanceDue = 0;
        int prevPage = 0;
        double totalecords = 12.0;
        private int numberOfItemsPerPage = 0;
        private int numberOfItemsPrintedSoFar = 0;
        private GroupBox gbox = new GroupBox();
        int gBoxX = 775;
        int gBoxY = 65;
        int gHeignt = 505;
        int gWidth = 480;
        string invoiceNo = "";
        private bool isFirstTimeLoadWindow = true;
        public SaleInvoicePizza()
        {

            InitializeComponent();
            HideLabelMsgs();
            DataTable recentOrderDt =  DataAccess.instance.SaleInvoice.GetMaxInvoiceNo();
            if(recentOrderDt.Rows.Count > 0)
            {
                invoiceNo =  recentOrderDt.Rows[0].Field<String>("InvoiceNo");
                if(string.IsNullOrEmpty(invoiceNo))
                {
                    invoiceNo = "0";
                }
                invoiceNoTxt.Text = (Convert.ToInt32(invoiceNo)+1).ToString();
            }
            GetAllProductCategories();
         //   GetAllProducts();
            //GetAllProductCategories();
        }
        private void HideLabelMsgs()
        {
           lblPhoneNoRequired.Hide();
        }
        //private bool ValidateForm()
        //{
        //    bool isValid = true;
        //    if(lblPhoneNoRequired.Text.Trim() == null)
        //    {
        //        isValid = false;
        //        lblPhoneNoRequired.Show();
        //    }
        //    return isValid;
        //}
        private void searchProdBtn_Click(object sender, EventArgs e)
        {
            ProductListForm pf = new ProductListForm(true);
            pf.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            pf.refForm = this;
            pf.ShowDialog(this);
        }

        public void handleSearch(DataGridViewRow row)
        {
            try
            {
                int StockAvail = Convert.ToInt32(row.Cells["StockAvailable"].Value);
                if (StockAvail > 0)
                {
                    invoiceGrid.Rows.Add(row.Cells["Id"].Value, row.Cells["Barcode"].Value, row.Cells["Title"].Value, row.Cells["Unit"].Value, row.Cells["SalePrice"].Value, row.Cells["PurchasePrice"].Value, 0, 1, Convert.ToInt32(row.Cells["SalePrice"].Value) * 1, StockAvail);
                    invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                    calcInvoice();
                }
            }
            catch (Exception ex) { }
        }
        private void prodCodeSrTxt_TextChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(prodCodeSrTxt.Text);

        }

        private async void updateGrid()
        {
            invTypeCombo.SelectedItem = "Sale";
            //DataTable csDT = DataAccess.instance.Customers.GetAll();
            DataTable csDT = null;
            await Task.Run(() =>
            {
                csDT = DataAccess.instance.Accounts.GetCustomerAccounts();
            });
            customerCombo.DisplayMember = "Title";
            customerCombo.ValueMember = "Id";
            customerCombo.DataSource = csDT;
            customerCombo.SelectedValue = 3;
            invoiceGrid.Focus();

            DataTable baDT = DataAccess.instance.Accounts.GetAllBankAccounts();
            mopBankCombo.DisplayMember = "Title";
            mopBankCombo.ValueMember = "Id";
            mopBankCombo.DataSource = baDT;
            invoiceGrid.Focus();
            loaderPicBox.Hide();

            //invoiceNoTxt.Text = DataAccess.instance.Utility.autoNextNumber("InvoiceNo").ToString();
        }

        private void invoiceGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (invoiceGrid.CurrentCell != null)
                {
                    loaderPicBox.Show();
                    DataGridViewRow invRow = (DataGridViewRow)invoiceGrid.Rows[e.RowIndex];
                    var currentCell = invoiceGrid.CurrentCell;
                    var columnName = invoiceGrid.Columns[e.ColumnIndex].Name;
                    string itemCode =  invoiceGrid.CurrentRow.Cells[invoiceGrid.Columns[1].Name].Value.ToString();
                    int qty = Convert.ToInt32(invoiceGrid.CurrentRow.Cells["quantity"].Value);
                    decimal purPrice = Convert.ToDecimal(invoiceGrid.CurrentRow.Cells["salePrice"].Value);
                    decimal rowSubTotal = purPrice * qty;
                    string discount = invoiceGrid.CurrentRow.Cells["discount"].Value.ToString();
                    DataTable itemDT = DataAccess.instance.Products.GetByItemCode(itemCode);
                    int stockAvail = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("StockAvailable"));
                    int stockAvail2 = Convert.ToInt32(invoiceGrid.CurrentRow.Cells["StockAvailable"].Value);
                   // int stockAvail = Convert.ToInt32(invoiceGrid.CurrentRow.Cells["stockAvailable"].Value);
                    if (qty > stockAvail)
                    {
                        MessageBox.Show("Exceeded Maximmum Stock");
                        invoiceGrid.CurrentRow.Cells["quantity"].Value = stockAvail;
                        qty = stockAvail;
                    }
                    if (discount.IndexOf("%") > -1)
                    {
                        char[] MyChar = { '%', ' ' };
                        decimal dis = Convert.ToDecimal(discount.Trim(MyChar));
                        invoiceGrid.CurrentRow.Cells["discount"].Value = rowSubTotal * dis / 100;
                    }

                    invoiceGrid.CurrentRow.Cells["amount"].Value = rowSubTotal - Convert.ToDecimal(invoiceGrid.CurrentRow.Cells["discount"].Value);
                    calcInvoice();
                    // subTotal.Text = invoiceGrid.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells["amount"].Value)).ToString();
                    //calcDiscount();
                }
            }
            catch (Exception ex) { }
        }
        private void DirectPrintInvoice(int saleInv) {
            loaderPicBox.Show();
            SaleInvoiceReportViewerForm saleInvForm = new SaleInvoiceReportViewerForm(saleInv);
            saleInvForm.Show(this);
            loaderPicBox.Hide();
            //PrintDialog printDialog = new PrintDialog();
            //if(printDialog.ShowDialog() == DialogResult.OK)
            //{
            //    CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocument = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            //    reportDocument.Load(Application.StartupPath + "\\Reports\\SaleInvoicePizzaReport1.rpt");
            //    DataTable recentOrderDt = null;

            //    recentOrderDt = DataAccess.instance.SaleInvoice.GetInvById(saleInv);

            //    reportDocument.SetDataSource(recentOrderDt);

            //    reportDocument.PrintOptions.PrinterName = printDialog.PrinterSettings.PrinterName;
            //    reportDocument.PrintToPrinter(printDialog.PrinterSettings.Copies, printDialog.PrinterSettings.Collate, printDialog.PrinterSettings.FromPage,printDialog.PrinterSettings.FromPage);

            //}
        }
        private void SaleInvoiceForm_Load(object sender, EventArgs e)
        {
            this.MinimumSize = new Size(1700, 900);
            mopBankCombo.Enabled = false;
            updateGrid();
            invoiceGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            invoiceGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            //invoiceGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);
            // this.reportViewer1.RefreshReport();

            prodCodeSrTxt.Select();
            this.ActiveControl = prodCodeSrTxt;
             prodCodeSrTxt.Focus();
        }

        private void SaleInvoiceForm_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void invoiceGrid_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.F12)
            //{
            //    this.discountInPrecent = !this.discountInPrecent;
            //    if(this.discountInPrecent)
            //    {
            //        this.discountStatus.Text = "Discount Type : Percentage";
            //    }
            //    else
            //    {
            //        this.discountStatus.Text = "Discount Type : Price";
            //    }
            //}
        }
        private void calcInvoice()
        {
            try
            {
                subTotalTxt.Text = invoiceGrid.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells["amount"].Value)).ToString();
                totalTxt.Text = (Convert.ToDecimal(subTotalTxt.Text) - Convert.ToDecimal(discountTxt.Text)).ToString();
                loaderPicBox.Hide();
            }
            catch (Exception ex) { }
        }
        private void prodCodeSrTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string itemCode = prodCodeSrTxt.Text;
                GetElementByBarcode(itemCode);
                //DataTable itemDT = DataAccess.instance.Products.GetByItemCode(itemCode);
                //if (itemDT.Rows.Count > 0)
                //{
                //    int Id = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("Id"));
                //    string Barcode = itemDT.Rows[0].Field<String>("Barcode");
                //    String Title = itemDT.Rows[0].Field<String>("Title");
                //    String Unit = itemDT.Rows[0].Field<String>("Unit");
                //    decimal PurchasePrice = itemDT.Rows[0].Field<decimal>("PurchasePrice");
                //    decimal SalePrice = itemDT.Rows[0].Field<decimal>("SalePrice");
                //    int stockAvail = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("StockAvailable"));
                //    if (stockAvail > 0)
                //    {
                //        invoiceGrid.Rows.Add(Id, Barcode, Title, Unit, SalePrice, PurchasePrice, 0, 1, SalePrice * 1);
                //        invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                //        calcInvoice();
                //    }
                //    else
                //    {
                //        MessageBox.Show("Stock not available");
                //    }
                //}
            }
        }
        private void GetElementByBarcode(string itemCode)
        {
            AddItemsToGrid(itemCode);
        }
        private void AddItemsToGrid(string itemCode)
        {
            DataTable itemDT = DataAccess.instance.Products.GetByItemCode(itemCode);
            DataRow row = itemDT.NewRow();
            if (itemDT.Rows.Count > 0)
            {
                int Id = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("Id"));
                string Barcode = itemDT.Rows[0].Field<String>("Barcode");
                String Title = itemDT.Rows[0].Field<String>("Title");
                String Unit = itemDT.Rows[0].Field<String>("Unit");
                decimal PurchasePrice = itemDT.Rows[0].Field<decimal>("PurchasePrice");
                decimal SalePrice = itemDT.Rows[0].Field<decimal>("SalePrice");
                int stockAvail = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("StockAvailable"));
                //row["Id"] = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("Id"));
                //row["Barcode"] = itemDT.Rows[0].Field<String>("Barcode");
                //row["Title"] = itemDT.Rows[0].Field<String>("Title");
                //row["Unit"] = itemDT.Rows[0].Field<String>("Unit");
                //row["PurchasePrice"] = itemDT.Rows[0].Field<decimal>("PurchasePrice");
                //row["SalePrice"] = itemDT.Rows[0].Field<decimal>("SalePrice");

                //  row["StockAvailable"] = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("StockAvailable"));
                if (stockAvail > 0)
                {
                    invoiceGrid.Rows.Add(Id, Barcode, Title, Unit, SalePrice, PurchasePrice, 0, 1, SalePrice * 1);
                  // invoiceGrid.Rows.Add(row);
                   // itemDT.AcceptChanges();
                   // invoiceGrid.DataSource = itemDT;
                  //  invoiceGrid.DataBind();
                    invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                    calcInvoice();
                }
                else
                {
                    MessageBox.Show("Stock not available");
                }
            }
        }
        private bool validateForm()
        {
            bool isValid = true;
            string invType = invTypeCombo.Text.Trim();
            if (invType == "")
            {
                MessageBox.Show("Select Invoice Type");
                //return false;
                isValid = false;
            }
            if (mobileNoTxt.Text.Trim() == "")
            {
                isValid = false;
                lblPhoneNoRequired.Show();
            }

            //if(this.invNoTxt.Text.Trim()== "")
            //{
            //    MessageBox.Show("Enter a valid Invoice#");
            //    return false;
            //}
            if (!DateTime.TryParse(this.dateCombo.Text, out var tempDate))
            {
                MessageBox.Show("Enter a valid Date");
                // return false;
                isValid = false;
            }
            int supplierAccId = Convert.ToInt32(this.customerCombo.SelectedValue);
            if (supplierAccId <= 0)
            {
                MessageBox.Show("Please Select customer");
                //return false;
                isValid = false;
            }

            Boolean mopBank = mopBankRadio.Checked;
            if (mopBank)
            {

                int bankId = Convert.ToInt32(mopBankCombo.SelectedValue);
                if (bankId <= 0)
                {
                    MessageBox.Show("Select a bank");
                    // return false;
                    isValid = false;
                }
            }
            if (invoiceGrid.Rows.Count <= 0)
            {
                MessageBox.Show("You can't save a zero item invoice");
                // return false;
                isValid = false;
            }
            return isValid;
        }
        private async void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateForm())
                {
               
                    calcInvoice();
                    string invNo = this.invoiceNoTxt.Text.Trim();
                    DateTime date = this.dateCombo.Value;
                    int customerId = Convert.ToInt32(this.customerCombo.SelectedValue);
                    string desc = this.descTxt.Text.Trim();
                    string FinalDis = this.discountTxt.Text.Trim();
                    decimal subTotal = Convert.ToDecimal(this.subTotalTxt.Text);
                    decimal total = Convert.ToDecimal(this.totalTxt.Text);
                    string invType = invTypeCombo.Text.Trim();
                    string mobileNo = mobileNoTxt.Text.Trim();
                    int saleInv = 0;
                    int badDebts = 0;
                    if (invType == "Sale")
                    {
                        await Task.Run(() =>
                        {
                            saleInv = DataAccess.instance.SaleInvoice.insert(invNo, customerId, date, desc, FinalDis, subTotal, total, mobileNo, DueAmount, BalanceDue);
                            badDebts = DataAccess.instance.SaleInvoice.InsertIntoBadDebits( customerId, date, total, mobileNo, DueAmount);

                        });
                        if (saleInv > 0)
                        {
                            foreach (DataGridViewRow row in invoiceGrid.Rows)
                            {
                                int pId = Convert.ToInt32(row.Cells["pId"].Value);
                                if (pId > 0)
                                {
                                    string itemName = row.Cells["itemName"].Value.ToString();
                                    string unit = row.Cells["unit"].Value.ToString();
                                    decimal purchasePrice = Convert.ToDecimal(row.Cells["purchasePrice"].Value);
                                    int quantity = Convert.ToInt32(row.Cells["quantity"].Value);
                                    decimal unitPr = Convert.ToDecimal(row.Cells["salePrice"].Value);
                                    DataAccess.instance.SaleInvoice.insertDetails(saleInv, pId, purchasePrice, quantity, unitPr);
                                }
                            }
                            int vId = 0;
                            int maxSaleInvoiceId = 0;
                            DataTable dtGetMaxId = DataAccess.instance.SaleInvoice.GetMaxInvoiceNo();
                            if (dtGetMaxId.Rows.Count > 0)
                            {
                                maxSaleInvoiceId = Convert.ToInt32(dtGetMaxId.Rows[0].Field<dynamic>("Id"));
                            }
                            //create sale type voucher
                            await Task.Run(() =>
                            {
                                 vId = DataAccess.instance.Voucher.insert(maxSaleInvoiceId, 3, saleInv, date, "Item Sold");
                            });
                            Boolean mopCash = mopCashRadio.Checked;
                            Boolean mopCredit = mopCreditRadio.Checked;
                            Boolean mopBank = mopBankRadio.Checked;
                            if (mopCash)
                            {
                                //Debit sale to cash account
                                await Task.Run(() =>
                                {
                                    DataAccess.instance.Voucher.insertDetails(maxSaleInvoiceId, 2, total, 0, "Debit against sale");
                                });
                            }
                            else if (mopCredit)
                            {
                                //Debit sale bill to customer
                                await Task.Run(() =>
                                {
                                    DataAccess.instance.Voucher.insertDetails(maxSaleInvoiceId, customerId, total, 0, "Debit against sale");
                                });

                            }
                            else if (mopBank)
                            {
                                int bankId = Convert.ToInt32(mopBankCombo.SelectedValue);
                                //Debit sale bill to bank
                                await Task.Run(() =>
                                {
                                    DataAccess.instance.Voucher.insertDetails(maxSaleInvoiceId, bankId, total, 0, "Debit against sale");
                                });

                            }
                            //get sale account by code
                            DataTable ivAcc = null;
                            await Task.Run(() =>
                            {
                                 ivAcc = DataAccess.instance.Accounts.GetAccByCode("SALE-001");
                            });
                            int ivAccId = Convert.ToInt32(ivAcc.Rows[0].Field<dynamic>("Id"));
                            //credit sale account
                            DataAccess.instance.Voucher.insertDetails(maxSaleInvoiceId, ivAccId, 0, subTotal, "credit against sale");
                            int fDis = Convert.ToInt32(FinalDis);
                            if (fDis > 0)
                            {
                                //if discount then debit to discount accouunt
                                await Task.Run(() =>
                                {
                                    DataTable disAcc = DataAccess.instance.Accounts.GetAccByCode("DISCOUNT-001");
                                    int disAccId = Convert.ToInt32(ivAcc.Rows[0].Field<dynamic>("Id"));
                                    DataAccess.instance.Voucher.insertDetails(maxSaleInvoiceId, disAccId, fDis, 0, "debit discount against sale");
                                });
                            }
                           
                            PrintInvoice(saleInv);
                            // invoiceGrid.DataSource = new DataTable();
                            invoiceGrid.Rows.Clear();
                            invoiceGrid.Refresh();
                            invoiceNoTxt.Text = (Convert.ToInt32(invoiceNoTxt.Text) + 1).ToString();
                            mobileNoTxt.Text = "";
                            descTxt.Text = "";
                            prodCodeSrTxt.Text = "";
                            // prodCodeSrTxt.Select();
                            // this.ActiveControl = prodCodeSrTxt;
                            prodCodeSrTxt.Focus();
                            ClearAllFields();
                            // MessageBox.Show("Invoice Created");

                            //DataTable recentOrderDt = null;
                            //await Task.Run(() =>
                            // {
                            //     recentOrderDt = DataAccess.instance.SaleInvoice.GetInvById(saleInv);

                            // });
                            // SaleInvoicePizzaReport sinvs = new SaleInvoicePizzaReport();
                            // sinvs.SetDataSource(recentOrderDt);
                            // crystalReportViewer1.ReportSource = sinvs;
                            // crystalReportViewer1.Zoom(1);

                        }
                    }

                   
                }
            }
            catch (Exception ex) { }

        }

        private void mopBankRadio_CheckedChanged(object sender, EventArgs e)
        {
            Boolean mopBank = mopBankRadio.Checked;
            if (mopBank)
            {
                mopBankCombo.Enabled = true;
            }
            else
            {
                mopBankCombo.Enabled = false;
            }
        }

        private void discountTxt_Validating(object sender, CancelEventArgs e)
        {
            string discount = discountTxt.Text.Trim();
            if (discount.IndexOf("%") > -1)
            {
                char[] MyChar = { '%', ' ' };
                decimal dis = Convert.ToDecimal(discount.Trim(MyChar));
                discountTxt.Text = (Convert.ToDecimal(subTotalTxt.Text) * dis / 100).ToString();
            }
            if (discount == "")
            {
                discountTxt.Text = "0";
            }
            calcInvoice();
        }

        private void mobileNoTxt_TextChanged(object sender, EventArgs e)
        {
            if (mobileNoTxt.Text.Trim().Length <=  0)
            {
                lblPhoneNoRequired.Show();
            }
            else
            {
                lblPhoneNoRequired.Hide();
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            decimal total = 0;
            decimal discount = 0;
            decimal grandTotal = 0;
            var recentOrderDt = DataAccess.instance.SaleInvoice.GetInvById(1080);
            List<SaleInvoiceModel> ProductList = new List<SaleInvoiceModel>();
            foreach (DataRow row in recentOrderDt.Rows)
            {
                var ProductObj = new SaleInvoiceModel()
                {
                    Title = row["Title"].ToString(),
                    Quantitiy = Convert.ToInt32(row["Quantitiy"]),
                    MobileNo = row["MobileNo"].ToString(),
                    SalePrice = Convert.ToInt32(row["SalePrice"]),
                    Total = Convert.ToInt32(row["Total"]),
                    SubTotal = Convert.ToInt32(row["SubTotal"]),
                    FinalDiscount = Convert.ToInt32(row["FinalDiscount"]),
                    Customer = row["Customer"].ToString(),
                };
                ProductList.Add(ProductObj);
            }
            
           // bool isEmptyOrNull = string.IsNullOrEmpty(etsalepaidamount.Text);
            bool isEmptyOrNull = false;
            if (!isEmptyOrNull)
            {
                // discount = Convert.ToDecimal(etsalepaidamount.Text);
                discount = 10;
            }
            Size formSize = this.ClientSize;
            //  Image image = Resources.SBLOGO;
            PrintPreviewControl pc = new PrintPreviewControl();

            // e.Graphics.DrawImage(image, 80, 90, 100, 100);

          //  e.Graphics.DrawString("Purchase Slip  " + etClientName.Text, new Font("Arial", 18, FontStyle.Bold),
            e.Graphics.DrawString("Purchase Slip  " + "", new Font("Arial", 18, FontStyle.Bold),
             Brushes.Black, new Point(330, 130));

            e.Graphics.DrawString("Date : " + DateTime.Now.ToShortDateString(), new Font("Arial", 16, FontStyle.Bold),
               Brushes.Black, new Point(25, 200));
            e.Graphics.DrawString("Mobile No :  " + (ProductList != null ? ProductList[0].MobileNo.ToString():""), new Font("Arial", 16, FontStyle.Bold),
               Brushes.Black, new Point(25, 230));
            e.Graphics.DrawString("---------------------------------------------------------------------------------------------" +
                "-------------------------------------------------"
                , new Font("Arial", 12, FontStyle.Regular),
               Brushes.Black, new Point(10, 270));
            e.Graphics.DrawString("Item Name", new Font("Arial", 16, FontStyle.Bold),
              Brushes.Black, new Point(150, 295));

            e.Graphics.DrawString("Quantity", new Font("Arial", 16, FontStyle.Bold),
             Brushes.Black, new Point(300, 295));

            e.Graphics.DrawString("Unit Price", new Font("Arial", 16, FontStyle.Bold),
               Brushes.Black, new Point(450, 295));

            e.Graphics.DrawString("Total Price", new Font("Arial", 16, FontStyle.Bold),
               Brushes.Black, new Point(600, 295));
            //e.Graphics.DrawString("Total Price", new Font("Arial", 12, FontStyle.Regular),
            //   Brushes.Black, new Point(700, 295));

            e.Graphics.DrawString("---------------------------------------------------------------------------------------------" +
                "-------------------------------------------------"
                , new Font("Arial", 12, FontStyle.Regular),
               Brushes.Black, new Point(10, 330));
            int yPos = 370;


            for (int i = numberOfItemsPrintedSoFar; i < ProductList.Count; i++)
                //for (int i = numberOfItemsPrintedSoFar; i < shoping.Count; i++)
            {
                numberOfItemsPerPage++;
                if (numberOfItemsPerPage <= 20)
                {
                    numberOfItemsPrintedSoFar++;
                    if (numberOfItemsPrintedSoFar <=30)
                    {
                        // e.Graphics.DrawString(shoping[i].barcode, new Font("Arial", 12, FontStyle.Regular),
                        //Brushes.Black, new Point(50, yPos));
                        e.Graphics.DrawString(ProductList[i].Title, new Font("Arial", 12, FontStyle.Regular),
                       Brushes.Black, new Point(150, yPos));
                        e.Graphics.DrawString(ProductList[i].Quantitiy.ToString(), new Font("Arial", 12, FontStyle.Regular),
                          Brushes.Black, new Point(300, yPos));
                        e.Graphics.DrawString(ProductList[i].SalePrice.ToString(), new Font("Arial", 12, FontStyle.Regular),
                           Brushes.Black, new Point(450, yPos));
                        e.Graphics.DrawString((ProductList[i].Quantitiy * ProductList[i].SalePrice).ToString(), new Font("Arial", 12, FontStyle.Regular),
                           Brushes.Black, new Point(600, yPos));
                        yPos += 30;

                        total = ProductList[i].Total;
                        discount = ProductList[i].FinalDiscount;
                        grandTotal = ProductList[i].SubTotal;
                    }

                    else
                    {
                        // numberOfItemsPerPage = 0;
                        e.HasMorePages = false;
                    }
                }
                else
                {
                    numberOfItemsPerPage = 0;
                    e.HasMorePages = true;
                    return;
                }
            }
            int again_ypos = yPos + 50;
            e.Graphics.DrawString("---------------------------------------------------------------------------------------------" +
               "-------------------------------------------------"
               , new Font("Arial", 12, FontStyle.Regular),
              Brushes.Black, new Point(10, again_ypos));

            e.Graphics.DrawString("Total :    " + total.ToString(), new Font("Arial", 16, FontStyle.Regular),
               Brushes.Black, new Point(520, again_ypos + 100));
            e.Graphics.DrawString("Discount :    " + (discount).ToString(), new Font("Arial", 16, FontStyle.Regular),
              Brushes.Black, new Point(520, again_ypos + 150));
            e.Graphics.DrawString("Grand Total :    " + grandTotal.ToString(), new Font("Arial", 16, FontStyle.Regular),
              Brushes.Black, new Point(520, again_ypos + 200));

            //decimal tax = 0;
            //if ("ettaxValue.Text" != "")
            //{
            //    //tax = Convert.ToDecimal(ettaxValue.Text) * ((Convert.ToDecimal(etsaletotalprice.Text)) / 100);
            //    tax = Convert.ToDecimal(23) * ((Convert.ToDecimal(500)) / 100);

            //}
            // var grandTotal = ((Convert.ToDecimal(5677)) - Convert.ToDecimal(5435) * (discount / 100));
            //grandTotal += tax;
            //e.Graphics.DrawString("Grand Total :    " + grandTotal.ToString(), new Font("Arial", 16, FontStyle.Regular),
            // Brushes.Black, new Point(520, again_ypos + 250));

            pc.Zoom = 0.25;
            pc.UseAntiAlias = true;

            numberOfItemsPerPage = 0;
            numberOfItemsPrintedSoFar = 0;
        }

       
        private void PrintInvoice(int saleInv)
        {
            DirectPrintInvoice(saleInv);
            //printPreviewDialog1.Document = printDocument1;
            //printPreviewDialog1.ShowDialog();
        }
        

     private void GetAllProductCategories()
        {
            DataTable dt = DataAccess.instance.ProductCategories.GetAll();
            DataRow dr;


            
           // DataTable dt = new DataTable();

            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--Show All Category--" };
            dt.Rows.InsertAt(dr, 0);

            categoryComboBox.ValueMember = "Id";

            categoryComboBox.DisplayMember = "Title";
            categoryComboBox.DataSource = dt;

           
        }

        private void GetAllProducts()
        {
           
            DataTable pDT = DataAccess.instance.Products.GetDetailed(1);
            PopulateGroupoxWithItms(pDT,true);

        }
        private void PopulateGroupoxWithItms(DataTable pDT,bool isFirst)
        {
           // float scrHeight = SystemInformation.VirtualScreen.Height;
            float scrWidth = SystemInformation.VirtualScreen.Width;
            if (!isFirst)
            {
                gbox.Controls.Clear();
                this.Controls.Remove(gbox);
            }
            if (scrWidth > 1900 && isFirst)
            {
                gBoxX = 1150;
                gBoxY = 65;
                gHeignt = 505;
                gWidth = 480;
            }
            //else
            if (scrWidth > 1900 && !isFirst)
            {

                gBoxX = 1410;
                gBoxY = 65;
                gHeignt = 505;
                gWidth = 480;
            }
            if (scrWidth > 1300 && scrWidth < 1600 && isFirst)
            {

              // gBoxX = 775;
                gBoxX = 855;
                gBoxY = 65;
                gHeignt = 505;
                gWidth = 480;

            }
            else if (scrWidth > 1300 && scrWidth < 1600  && !isFirst)
            {
                gBoxX = 855;
                gBoxY = 65;
                gHeignt = 505;
                gWidth = 480;
               
            }

            int TotalItems = 0;
            int fetchItems = pDT.Rows.Count;
            if (fetchItems > 0)
            {
                TotalItems = Convert.ToInt32(pDT.Rows[0]["TotalCount"]);
            }
            double pages =Convert.ToDouble( TotalItems) / totalecords;
            lblTotalItemsPage.Text = Math.Ceiling(pages).ToString();
            gbox.Location = new Point(gBoxX, gBoxY);
            // gbox.Size = new Size(430, 570);
            //  gbox.Font = new Font("Colonna MT", 12);
            gbox.Text = "Select Items";
            gbox.Visible = true;
            //gbox.AutoSize = true;
            gbox.Size = new Size(gHeignt, gWidth);

            gbox.AutoSizeMode = AutoSizeMode.GrowOnly;
            gbox.AutoSize = false;
            gbox.Dock = DockStyle.None;
            gbox.Padding = new Padding(3, 3, 3, 3);
            gbox.Margin = new Padding(3, 3, 3, 3);

            // gbox.TextAlign = ContentAlignment.;
            gbox.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            this.Controls.Add(gbox);
            int x = -25, y = -40;
            int Orignal = -25;
            int width = 95;
            int height = 72;
            for (int i = 0; i < fetchItems; i++)
            {
                string name = pDT.Rows[i]["Title"].ToString();
                string barcode = pDT.Rows[i]["Barcode"].ToString();
                var xAxis = 125;
                if (i % 4 != 0)
                {

                    // if (i != 0) xAxis = 170;
                    x = x + xAxis; // create button on right side

                    Button b = new Button();
                    b.Location = new Point(x, y);
                    b.Name = barcode;
                    b.Text = name;
                    b.Size = new Size(width, height);
                    b.Font = new Font("Minion Pro", 13);
                    b.Padding = new Padding(2);
                    b.Parent = gbox;
                    // b.Image=Image.
                    b.Click += new EventHandler(button_Click);
                    gbox.Controls.Add(b);
                }
                else
                {

                    x = Orignal + 40;/* create button on bottom of first */
                    y = y + 100;


                    Button b = new Button();
                    b.Location = new Point(x, y);
                    b.Name = barcode;
                    b.Text = name;
                    b.Parent = gbox;
                    b.Size = new Size(width, height);
                    b.Font = new Font("Minion Pro", 15);
                    b.Padding = new Padding(2);
                    b.Click += new EventHandler(button_Click);
                    gbox.Controls.Add(b);
                }
            }
            prodCodeSrTxt.Select();
            this.ActiveControl = prodCodeSrTxt;
            lblPreviousPage.Text = "1";
        }

        protected void button_Click(object sender, EventArgs e)
        {
                Button button = sender as Button;
                GetElementByBarcode(button.Name);
        }

        private void button3_Click(object sender, EventArgs e)
        {
             totalPage = Convert.ToInt32(lblTotalItemsPage.Text);
             prevPage = Convert.ToInt32(lblPreviousPage.Text);
            if (prevPage < totalPage)
            {
               
                int categoryId = Convert.ToInt32(categoryComboBox.SelectedValue);
                GetProductByCategoryIdWithPagination(categoryId, Convert.ToInt32(totalecords) * prevPage-1 <= 0 ? 1 : Convert.ToInt32(totalecords) * prevPage - 1);
                prevPage += 1;
                lblPreviousPage.Text = prevPage + "";
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            totalPage = Convert.ToInt32(lblTotalItemsPage.Text);
            prevPage = Convert.ToInt32(lblPreviousPage.Text);
            if (prevPage > 1)
            {
                prevPage -= 1;
                int categoryId = Convert.ToInt32(categoryComboBox.SelectedValue);
                var res = Convert.ToInt32(totalecords) * prevPage + 1 <= 0 ? 1 : Math.Ceiling(((totalecords * Convert.ToInt32(prevPage+1)) / totalecords));
                GetProductByCategoryIdWithPagination(categoryId,Convert.ToInt32(res)-1);
               

                lblPreviousPage.Text = prevPage + "";
            }
            //DataTable pDT = DataAccess.instance.Products.GetDetailed();
            //PopulateGroupoxWithItms(pDT, false);
        }

        private void categoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (isFirstTimeLoadWindow)
            //{
            //    GetAllProducts();
            //    isFirstTimeLoadWindow = false;
            //}
            //else
            //{
                int categoryId = Convert.ToInt32(categoryComboBox.SelectedValue);
                GetProductByCategoryIdWithPagination(categoryId, 1);
           
                //if (categoryId > 0)
                //{
                //    DataTable pDT = DataAccess.instance.Products.GetProductByCategoryId(categoryId);
                //    PopulateGroupoxWithItms(pDT, false);
                //}
                //else
                //{

                //    DataTable pDT = DataAccess.instance.Products.GetDetailed();
                //    PopulateGroupoxWithItms(pDT, false);
                //}
           // }
            
        }
        private void GetProductByCategoryIdWithPagination(int categoryId,int pageNo)
        {
            if (isFirstTimeLoadWindow)
            {
                GetAllProducts();
                isFirstTimeLoadWindow = false;
            }
            else
            {
               // int categoryId = Convert.ToInt32(categoryComboBox.SelectedValue);
               
                if (categoryId > 0)
                {
                    DataTable pDT = DataAccess.instance.Products.GetProductByCategoryId(categoryId,pageNo);
                    PopulateGroupoxWithItms(pDT, false);
                }
                else
                {

                    DataTable pDT = DataAccess.instance.Products.GetDetailed(pageNo);
                    PopulateGroupoxWithItms(pDT, false);
                }
            }
        }
        private void prodCodeSrTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                AddItemsToGrid(prodCodeSrTxt.Text);
            }
        }

        private void txtPay_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void discountTxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(discountTxt.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                discountTxt.Text = discountTxt.Text.Remove(discountTxt.Text.Length - 1);
            }
        }

        private void txtReceived_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtBalance.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                txtReceived.Text = txtReceived.Text.Remove(txtReceived.Text.Length - 1);
            }
           
        }

        private void txtReceived_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                    if (!string.IsNullOrEmpty(totalTxt.Text))
                    {
                        var totalAmount = Convert.ToDecimal(totalTxt.Text);

                        if (!string.IsNullOrEmpty(txtReceived.Text) && totalAmount > 0)
                        {
                            var PayAmount = Convert.ToDecimal(txtReceived.Text);
                            if (PayAmount - totalAmount < 0)
                            {
                                DueAmount = PayAmount - totalAmount;
                                txtDue.Text = DueAmount.ToString();
                                txtBalance.Text = "0";
                                BalanceDue = 0;
                            }
                            else if (PayAmount - totalAmount > 0)
                            {
                                BalanceDue = PayAmount - totalAmount;
                                txtBalance.Text = BalanceDue.ToString();
                                txtDue.Text = "0";
                                DueAmount = 0;
                            }
                            else if (PayAmount - totalAmount == 0)
                            {
                                BalanceDue = 0;
                                DueAmount = 0;
                                txtDue.Text = "0";
                                txtBalance.Text = "0";
                            }
                        }
                        else
                        {
                            txtReceived.Text = "";
                            txtDue.Text = "0";
                            txtBalance.Text = "0";
                        }
                    }
                }
            }
        private void ClearAllFields()
        {
            BalanceDue = 0;
            DueAmount = 0;
            txtBalance.Text = "0";
            txtReceived.Text = "";
            txtDue.Text = "0";
            txtBalance.Text = "0";
            totalTxt.Text = "0";
            discountTxt.Text = "0";
            subTotalTxt.Text = "0";
        }
    }
}
