﻿namespace EXYPOS.TransactionForms
{
    partial class PurchaseInvoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseInvoiceForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchProdBtn = new System.Windows.Forms.Button();
            this.prodCodeSrTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.loaderPicBox = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.descTxt = new System.Windows.Forms.TextBox();
            this.supplierCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateCombo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.invNoTxt = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.mopBankCombo = new System.Windows.Forms.ComboBox();
            this.mopBankRadio = new System.Windows.Forms.RadioButton();
            this.mopCashRadio = new System.Windows.Forms.RadioButton();
            this.mopCreditRadio = new System.Windows.Forms.RadioButton();
            this.discountTxt = new System.Windows.Forms.TextBox();
            this.subTotalTxt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.totalTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.invoiceGrid = new System.Windows.Forms.DataGridView();
            this.pId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchasePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.invTypeCombo = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.searchProdBtn);
            this.groupBox1.Controls.Add(this.prodCodeSrTxt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 183);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(801, 69);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Product";
            // 
            // searchProdBtn
            // 
            this.searchProdBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.searchProdBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchProdBtn.BackgroundImage")));
            this.searchProdBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchProdBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchProdBtn.Location = new System.Drawing.Point(323, 21);
            this.searchProdBtn.Name = "searchProdBtn";
            this.searchProdBtn.Size = new System.Drawing.Size(30, 30);
            this.searchProdBtn.TabIndex = 18;
            this.searchProdBtn.UseVisualStyleBackColor = false;
            this.searchProdBtn.Click += new System.EventHandler(this.searchProdBtn_Click);
            // 
            // prodCodeSrTxt
            // 
            this.prodCodeSrTxt.Location = new System.Drawing.Point(95, 26);
            this.prodCodeSrTxt.Name = "prodCodeSrTxt";
            this.prodCodeSrTxt.Size = new System.Drawing.Size(222, 20);
            this.prodCodeSrTxt.TabIndex = 1;
            this.prodCodeSrTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.prodCodeSrTxt_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Item/Barcode";
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(108, 361);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(108, 39);
            this.saveBtn.TabIndex = 15;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.loaderPicBox);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.descTxt);
            this.groupBox2.Location = new System.Drawing.Point(12, 82);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(800, 95);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Invoice Details";
            // 
            // loaderPicBox
            // 
            this.loaderPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loaderPicBox.Image = ((System.Drawing.Image)(resources.GetObject("loaderPicBox.Image")));
            this.loaderPicBox.Location = new System.Drawing.Point(714, 13);
            this.loaderPicBox.Name = "loaderPicBox";
            this.loaderPicBox.Size = new System.Drawing.Size(80, 58);
            this.loaderPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loaderPicBox.TabIndex = 16;
            this.loaderPicBox.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Description";
            // 
            // descTxt
            // 
            this.descTxt.Location = new System.Drawing.Point(95, 19);
            this.descTxt.Multiline = true;
            this.descTxt.Name = "descTxt";
            this.descTxt.Size = new System.Drawing.Size(343, 52);
            this.descTxt.TabIndex = 15;
            // 
            // supplierCombo
            // 
            this.supplierCombo.FormattingEnabled = true;
            this.supplierCombo.Location = new System.Drawing.Point(642, 20);
            this.supplierCombo.Name = "supplierCombo";
            this.supplierCombo.Size = new System.Drawing.Size(128, 21);
            this.supplierCombo.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(581, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Supplier";
            // 
            // dateCombo
            // 
            this.dateCombo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateCombo.Location = new System.Drawing.Point(431, 20);
            this.dateCombo.Name = "dateCombo";
            this.dateCombo.Size = new System.Drawing.Size(127, 20);
            this.dateCombo.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(376, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(154, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Invoice #";
            // 
            // invNoTxt
            // 
            this.invNoTxt.Location = new System.Drawing.Point(229, 20);
            this.invNoTxt.Name = "invNoTxt";
            this.invNoTxt.ReadOnly = true;
            this.invNoTxt.Size = new System.Drawing.Size(124, 20);
            this.invNoTxt.TabIndex = 7;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.discountTxt);
            this.groupBox3.Controls.Add(this.saveBtn);
            this.groupBox3.Controls.Add(this.subTotalTxt);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.totalTxt);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(838, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 555);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Payment Summary";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.mopBankCombo);
            this.groupBox5.Controls.Add(this.mopBankRadio);
            this.groupBox5.Controls.Add(this.mopCashRadio);
            this.groupBox5.Controls.Add(this.mopCreditRadio);
            this.groupBox5.Location = new System.Drawing.Point(31, 241);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 100);
            this.groupBox5.TabIndex = 28;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "MOP";
            // 
            // mopBankCombo
            // 
            this.mopBankCombo.FormattingEnabled = true;
            this.mopBankCombo.Location = new System.Drawing.Point(9, 61);
            this.mopBankCombo.Name = "mopBankCombo";
            this.mopBankCombo.Size = new System.Drawing.Size(176, 21);
            this.mopBankCombo.TabIndex = 27;
            // 
            // mopBankRadio
            // 
            this.mopBankRadio.AutoSize = true;
            this.mopBankRadio.Location = new System.Drawing.Point(135, 25);
            this.mopBankRadio.Name = "mopBankRadio";
            this.mopBankRadio.Size = new System.Drawing.Size(50, 17);
            this.mopBankRadio.TabIndex = 26;
            this.mopBankRadio.Text = "Bank";
            this.mopBankRadio.UseVisualStyleBackColor = true;
            this.mopBankRadio.CheckedChanged += new System.EventHandler(this.mopBankRadio_CheckedChanged);
            // 
            // mopCashRadio
            // 
            this.mopCashRadio.AutoSize = true;
            this.mopCashRadio.Checked = true;
            this.mopCashRadio.Location = new System.Drawing.Point(9, 25);
            this.mopCashRadio.Name = "mopCashRadio";
            this.mopCashRadio.Size = new System.Drawing.Size(49, 17);
            this.mopCashRadio.TabIndex = 24;
            this.mopCashRadio.TabStop = true;
            this.mopCashRadio.Text = "Cash";
            this.mopCashRadio.UseVisualStyleBackColor = true;
            // 
            // mopCreditRadio
            // 
            this.mopCreditRadio.AutoSize = true;
            this.mopCreditRadio.Location = new System.Drawing.Point(77, 25);
            this.mopCreditRadio.Name = "mopCreditRadio";
            this.mopCreditRadio.Size = new System.Drawing.Size(52, 17);
            this.mopCreditRadio.TabIndex = 25;
            this.mopCreditRadio.Text = "Credit";
            this.mopCreditRadio.UseVisualStyleBackColor = true;
            // 
            // discountTxt
            // 
            this.discountTxt.BackColor = System.Drawing.SystemColors.Window;
            this.discountTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountTxt.Location = new System.Drawing.Point(31, 121);
            this.discountTxt.Name = "discountTxt";
            this.discountTxt.Size = new System.Drawing.Size(185, 29);
            this.discountTxt.TabIndex = 16;
            this.discountTxt.Text = "0";
            this.discountTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.discountTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.discountTxt_KeyUp);
            this.discountTxt.Validating += new System.ComponentModel.CancelEventHandler(this.discountTxt_Validating);
            // 
            // subTotalTxt
            // 
            this.subTotalTxt.BackColor = System.Drawing.SystemColors.Window;
            this.subTotalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subTotalTxt.Location = new System.Drawing.Point(31, 58);
            this.subTotalTxt.Name = "subTotalTxt";
            this.subTotalTxt.ReadOnly = true;
            this.subTotalTxt.Size = new System.Drawing.Size(185, 29);
            this.subTotalTxt.TabIndex = 14;
            this.subTotalTxt.Text = "0";
            this.subTotalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Total";
            // 
            // totalTxt
            // 
            this.totalTxt.BackColor = System.Drawing.Color.MidnightBlue;
            this.totalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTxt.ForeColor = System.Drawing.SystemColors.Window;
            this.totalTxt.Location = new System.Drawing.Point(31, 199);
            this.totalTxt.Name = "totalTxt";
            this.totalTxt.ReadOnly = true;
            this.totalTxt.Size = new System.Drawing.Size(185, 29);
            this.totalTxt.TabIndex = 12;
            this.totalTxt.Text = "0";
            this.totalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Discount";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sub Total";
            // 
            // invoiceGrid
            // 
            this.invoiceGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.invoiceGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.invoiceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invoiceGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.invoiceGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.invoiceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.invoiceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.invoiceGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pId,
            this.itemBarcode,
            this.itemName,
            this.unit,
            this.purchasePrice,
            this.quantity,
            this.amount,
            this.delete});
            this.invoiceGrid.EnableHeadersVisualStyles = false;
            this.invoiceGrid.Location = new System.Drawing.Point(12, 258);
            this.invoiceGrid.Name = "invoiceGrid";
            this.invoiceGrid.RowHeadersVisible = false;
            this.invoiceGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.invoiceGrid.Size = new System.Drawing.Size(800, 309);
            this.invoiceGrid.TabIndex = 16;
            this.invoiceGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.invoiceGrid_CellContentClick);
            this.invoiceGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.invoiceGrid_CellValueChanged);
            // 
            // pId
            // 
            this.pId.HeaderText = "Id";
            this.pId.Name = "pId";
            this.pId.ReadOnly = true;
            // 
            // itemBarcode
            // 
            this.itemBarcode.HeaderText = "Item/Barcode";
            this.itemBarcode.Name = "itemBarcode";
            this.itemBarcode.ReadOnly = true;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Item Name";
            this.itemName.Name = "itemName";
            this.itemName.ReadOnly = true;
            // 
            // unit
            // 
            this.unit.HeaderText = "Unit";
            this.unit.Name = "unit";
            this.unit.ReadOnly = true;
            // 
            // purchasePrice
            // 
            this.purchasePrice.HeaderText = "Purchase Price";
            this.purchasePrice.Name = "purchasePrice";
            // 
            // quantity
            // 
            this.quantity.HeaderText = "Quantity";
            this.quantity.Name = "quantity";
            // 
            // amount
            // 
            this.amount.HeaderText = "Amount";
            this.amount.Name = "amount";
            this.amount.ReadOnly = true;
            // 
            // delete
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle3.NullValue")));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(2);
            this.delete.DefaultCellStyle = dataGridViewCellStyle3;
            this.delete.HeaderText = "";
            this.delete.Image = global::EXYPOS.Properties.Resources.delete;
            this.delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.delete.Name = "delete";
            this.delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.delete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // invTypeCombo
            // 
            this.invTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.invTypeCombo.FormattingEnabled = true;
            this.invTypeCombo.Items.AddRange(new object[] {
            "Purchase",
            "Purchase Return"});
            this.invTypeCombo.Location = new System.Drawing.Point(23, 19);
            this.invTypeCombo.Name = "invTypeCombo";
            this.invTypeCombo.Size = new System.Drawing.Size(121, 21);
            this.invTypeCombo.TabIndex = 14;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.invTypeCombo);
            this.groupBox4.Controls.Add(this.invNoTxt);
            this.groupBox4.Controls.Add(this.supplierCombo);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.dateCombo);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(800, 64);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Invoice Type";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 570);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1093, 22);
            this.statusStrip1.TabIndex = 19;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // PurchaseInvoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1093, 592);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.invoiceGrid);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PurchaseInvoiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Purchase Invoice";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PurchaseInvoiceForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox invNoTxt;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView invoiceGrid;
        private System.Windows.Forms.Button searchProdBtn;
        private System.Windows.Forms.TextBox prodCodeSrTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox descTxt;
        private System.Windows.Forms.ComboBox supplierCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox totalTxt;
        private System.Windows.Forms.ComboBox invTypeCombo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox discountTxt;
        private System.Windows.Forms.TextBox subTotalTxt;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pId;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchasePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount;
        private System.Windows.Forms.DataGridViewImageColumn delete;
        private System.Windows.Forms.ComboBox mopBankCombo;
        private System.Windows.Forms.RadioButton mopBankRadio;
        private System.Windows.Forms.RadioButton mopCreditRadio;
        private System.Windows.Forms.RadioButton mopCashRadio;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox loaderPicBox;
    }
}