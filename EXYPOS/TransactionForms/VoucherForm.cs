﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.TransactionForms
{
    public partial class VoucherForm : Form
    {
        public VoucherForm()
        {
            InitializeComponent();
        }

        private void VoucherForm_Load(object sender, EventArgs e)
        {

            updateGrid();
            invoiceGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            invoiceGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }
        private void updateGrid()
        {
            DataTable accDT = DataAccess.instance.Accounts.GetAll();
            accCombo.DisplayMember = "Title";
            accCombo.ValueMember = "Id";
            accCombo.DataSource = accDT;

            DataTable vTypeDT = DataAccess.instance.Voucher.GetVoucherTypes();
            voucherTypeCombo.DisplayMember = "Type";
            voucherTypeCombo.ValueMember = "Id";
            voucherTypeCombo.DataSource = vTypeDT;
            loaderPicBox.Hide();
            // voucherNoTxt.Text = DataAccess.instance.Utility.autoNextNumber("VoucherNo").ToString();
        }
        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void addTransactionBtn_Click(object sender, EventArgs e)
        {
            int accId = Convert.ToInt32(accCombo.SelectedValue);
            string accTitle = accCombo.Text.Trim();
            string remarks = remarksTxt.Text;
            decimal debit = debitTxt.Text.Trim().Equals("") ? 0 : Convert.ToDecimal(debitTxt.Text.Trim());
            decimal credit = creditTxt.Text.Trim().Equals("") ? 0 : Convert.ToDecimal(creditTxt.Text.Trim());
            invoiceGrid.Rows.Add(accId,accTitle,remarks,debit,credit);
        }

        private void invoiceGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = this.invoiceGrid.Columns[e.ColumnIndex].Name;
            switch (colName)
            {
                case "delete":
                    this.invoiceGrid.Rows.RemoveAt(e.RowIndex);
                    break;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                loaderPicBox.Show();
                int voucherTypeID = Convert.ToInt32(voucherTypeCombo.SelectedValue);
                int voucherNo = voucherNoTxt.Text.Trim().Equals("") ? 0 : Convert.ToInt32(voucherNoTxt.Text);
                DateTime date = dateCombo.Value;
                string desc = descTxt.Text;
                int vId = DataAccess.instance.Voucher.insert(voucherNo, voucherTypeID, 0, date, desc);
                if (vId > 0)
                {
                    foreach (DataGridViewRow row in invoiceGrid.Rows)
                    {
                        int accId = Convert.ToInt32(row.Cells["accId"].Value);
                        if (accId > 0)
                        {
                            string accTitle = row.Cells["accTitle"].Value.ToString();
                            string remarks = row.Cells["remarks"].Value.ToString();
                            decimal debit = Convert.ToDecimal(row.Cells["debit"].Value);
                            decimal credit = Convert.ToDecimal(row.Cells["credit"].Value);
                            DataAccess.instance.Voucher.insertDetails(vId, accId, debit, credit, remarks);
                            loaderPicBox.Hide();
                            MessageBox.Show("Voucher Saved");
                        }
                    }
                }
            }
            catch (Exception ex) {
                loaderPicBox.Hide();
            }
            
        }
    }
}
