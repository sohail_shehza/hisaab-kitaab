﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.TransactionForms
{
    public partial class PurchaseInvoiceForm : Form
    {
        string invoiceNo = "";
        public PurchaseInvoiceForm()
        {
            InitializeComponent();
            DataTable recentOrderDt = DataAccess.instance.PurchaseInvoice.GetMaxInvoiceNo();
            if (recentOrderDt.Rows.Count > 0)
            {
                invoiceNo = recentOrderDt.Rows[0].Field<String>("InvoiceNo");
                invNoTxt.Text = (Convert.ToInt32(invoiceNo) + 1).ToString();
            }
            else
            {
                invNoTxt.Text = "1";
            }
            this.ActiveControl = prodCodeSrTxt;
            prodCodeSrTxt.Focus();
        }
        private void updateGrid()
        {
            DataTable baDT = DataAccess.instance.Accounts.GetAllBankAccounts();
            mopBankCombo.DisplayMember = "Title";
            mopBankCombo.ValueMember = "Id";
            mopBankCombo.DataSource = baDT;
            invoiceGrid.Focus();

            DataTable supDT = DataAccess.instance.Accounts.GetSupplierAccounts();
            supplierCombo.DisplayMember = "Title";
            supplierCombo.ValueMember = "Id";
            supplierCombo.DataSource = supDT;

           // invNoTxt.Text = DataAccess.instance.Utility.autoNextNumber("InvoiceNo").ToString();

            invoiceGrid.Focus();
            loaderPicBox.Hide();
        }
        private void searchProdBtn_Click(object sender, EventArgs e)
        {
            ProductListForm pf = new ProductListForm(true);
            pf.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            pf.refForm = this;
            pf.ShowDialog(this);
        }
        public void handleSearch(DataGridViewRow row)
        {
            try
            {

                invoiceGrid.Rows.Add(row.Cells["Id"].Value,row.Cells["Barcode"].Value, row.Cells["Title"].Value, row.Cells["Unit"].Value, row.Cells["PurchasePrice"].Value, 1,Convert.ToDecimal(row.Cells["PurchasePrice"].Value) * 1);
                invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                calcInvoice();
            }
            catch (Exception ex) { }
        }
        private void calcInvoice()
        {
            try
            {
                subTotalTxt.Text = invoiceGrid.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells["amount"].Value)).ToString();
                totalTxt.Text = (Convert.ToDecimal(subTotalTxt.Text)  - Convert.ToDecimal(discountTxt.Text)).ToString();
            }
            catch (Exception ex) { }
        }
        private void invoiceGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (invoiceGrid.CurrentCell != null)
                {
                    int qty = Convert.ToInt32(invoiceGrid.CurrentRow.Cells["quantity"].Value);
                    decimal purPrice = Convert.ToDecimal(invoiceGrid.CurrentRow.Cells["purchasePrice"].Value);
                    invoiceGrid.CurrentRow.Cells["amount"].Value = purPrice * qty;
                }
                calcInvoice();
            }
            catch (Exception ex) { }
        }

        private Boolean validateForm()
        {
            string invType = invTypeCombo.Text.Trim();
            if (invType == "")
            {
                MessageBox.Show("Select Invoice Type");
                return false;
            }
            
            //if(this.invNoTxt.Text.Trim()== "")
            //{
            //    MessageBox.Show("Enter a valid Invoice#");
            //    return false;
            //}
            if (!DateTime.TryParse(this.dateCombo.Text,out var tempDate))
            {
                MessageBox.Show("Enter a valid Date");
                return false;
            }
            int supplierAccId = Convert.ToInt32(this.supplierCombo.SelectedValue);
            if (supplierAccId <= 0)
            {
                MessageBox.Show("Please Select supplier");
                return false;
            }
            
            Boolean mopBank = mopBankRadio.Checked;
            if(mopBank)
            {

                int bankId = Convert.ToInt32(mopBankCombo.SelectedValue);
                if (bankId <= 0)
                {
                    MessageBox.Show("Select a bank");
                    return false;
                }
            }
            if(invoiceGrid.Rows.Count<=0)
            {
                MessageBox.Show("You can't save a zero item invoice");
                return false;
            }
            return true;
        }
        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                calcInvoice();
                string invNo = this.invNoTxt.Text.Trim();
                DateTime date = this.dateCombo.Value;
                int supplierAccId = Convert.ToInt32(this.supplierCombo.SelectedValue);
                //int cashBankId = Convert.ToInt32(this.cashBankCombo.SelectedValue);
                string desc = this.descTxt.Text.Trim();
                string invType = invTypeCombo.Text.Trim();
                if (invType == "Purchase")
                {
                    decimal FinalDis = Convert.ToDecimal(this.discountTxt.Text.Trim());
                    decimal subTotal = Convert.ToDecimal(this.subTotalTxt.Text);
                    decimal total = Convert.ToDecimal(this.totalTxt.Text);
                    int pInv = DataAccess.instance.PurchaseInvoice.insert(invNo, supplierAccId, date, desc, subTotal, FinalDis, total);

                    if (pInv > 0)
                    {
                        foreach (DataGridViewRow row in invoiceGrid.Rows)
                        {
                            int pId = Convert.ToInt32(row.Cells["pId"].Value);
                            if (pId > 0)
                            {
                                string itemName = row.Cells["itemName"].Value.ToString();
                                string unit = row.Cells["unit"].Value.ToString();
                                decimal purchasePrice = Convert.ToDecimal(row.Cells["purchasePrice"].Value);
                                int quantity = Convert.ToInt32(row.Cells["quantity"].Value);
                                DataAccess.instance.PurchaseInvoice.insertDetails(pInv, pId, purchasePrice, quantity);
                            }
                        }
                        decimal Total = Convert.ToDecimal(totalTxt.Text);
                        //create purchase type voucher
                        DataTable getVIdNo = DataAccess.instance.PurchaseInvoice.GetMaxInvoiceNo();
                        int VoucherNo = 0;
                        if (getVIdNo !=  null)
                         VoucherNo = Convert.ToInt32(getVIdNo.Rows[0].Field<dynamic>("Id"));
                        int vId = DataAccess.instance.Voucher.insert(VoucherNo, 4, pInv, date, "Item Purchased");
                        Boolean mopCash = mopCashRadio.Checked;
                        Boolean mopCredit = mopCreditRadio.Checked;
                        Boolean mopBank = mopBankRadio.Checked;
                        if (mopCash)
                        {
                            //Credit purchase bill to cash account
                            DataAccess.instance.Voucher.insertDetails(VoucherNo, 2, 0, Total, "Debit against sale");
                        }
                        else if (mopCredit)
                        {
                            //Credit purchase bill to supplier
                            DataAccess.instance.Voucher.insertDetails(VoucherNo, supplierAccId, 0, Total, "Credit against purchase");

                        }
                        else if (mopBank)
                        {
                            int bankId = Convert.ToInt32(mopBankCombo.SelectedValue);
                            //Credit purchase bill to bank
                            DataAccess.instance.Voucher.insertDetails(VoucherNo, bankId, 0, Total, "Debit against sale");

                        }

                        //get purchase account by code
                        DataTable ivAcc = DataAccess.instance.Accounts.GetAccByCode("PURCHASE-001");
                        int ivAccId = Convert.ToInt32(ivAcc.Rows[0].Field<dynamic>("Id"));
                        //Debit purchase account
                        DataAccess.instance.Voucher.insertDetails(VoucherNo, ivAccId, Total, 0, "Debit against purchase");
                        invNoTxt.Text = (Convert.ToInt32(invNoTxt.Text) + 1).ToString();
                        //invoiceGrid.DataSource = new DataTable();
                         invoiceGrid.Rows.Clear();
                            invoiceGrid.Refresh();
                        MessageBox.Show("Invoice Created");
                        prodCodeSrTxt.Text = "";
                        prodCodeSrTxt.Focus();
                    }
                }
                else if (invType == "Purchase Return")
                {
                    MessageBox.Show("purchase return return");
                }
            }
        }

        private void PurchaseInvoiceForm_Load(object sender, EventArgs e)
        {
            mopBankCombo.Enabled = false;
            invTypeCombo.SelectedItem = "Purchase";
            updateGrid();
            invoiceGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            invoiceGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

        }

       

        private void expenseTxt_KeyUp(object sender, KeyEventArgs e)
        {
            calcInvoice();
        }

        private void discountTxt_KeyUp(object sender, KeyEventArgs e)
        {
            calcInvoice();
        }

        private void discountTxt_Validating(object sender, CancelEventArgs e)
        {
            string discount = discountTxt.Text.Trim();
            if (discount.IndexOf("%") > -1)
            {
                char[] MyChar = { '%', ' ' };
                decimal dis = Convert.ToDecimal(discount.Trim(MyChar));
                discountTxt.Text = (Convert.ToDecimal(subTotalTxt.Text) * dis / 100).ToString();
            }
            if (discount == "")
            {
                discountTxt.Text = "0";
            }
            calcInvoice();
        }


        private void invoiceGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = this.invoiceGrid.Columns[e.ColumnIndex].Name;
            switch (colName)
            {
                case "delete":
                    this.invoiceGrid.Rows.RemoveAt(e.RowIndex);
                    break;
            }
        }


        private void prodCodeSrTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                string itemCode = prodCodeSrTxt.Text;
                DataTable itemDT = DataAccess.instance.Products.GetByItemCode(itemCode);
                if(itemDT.Rows.Count > 0)
                {
                    int Id = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("Id"));
                    string Barcode = itemDT.Rows[0].Field<String>("Barcode");
                    String Title=itemDT.Rows[0].Field<String>("Title");
                    String Unit = itemDT.Rows[0].Field<String>("Unit"); 
                    decimal PurchasePrice = itemDT.Rows[0].Field<decimal>("PurchasePrice");
                    int stockAvail = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("StockAvailable"));
                    //if (stockAvail > 0)
                    //{
                        invoiceGrid.Rows.Add(Id, Barcode, Title, Unit, PurchasePrice, 1, PurchasePrice * 1);
                        invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                        calcInvoice();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Stock not available");
                    //}
                }
            }
        }

        private void mopBankRadio_CheckedChanged(object sender, EventArgs e)
        {
            Boolean mopBank = mopBankRadio.Checked;
            if (mopBank)
            {
                mopBankCombo.Enabled = true;
            }
            else
            {
                mopBankCombo.Enabled = false;
            }
        }
    }
}
