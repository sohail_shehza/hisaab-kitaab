﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.TransactionForms
{
    public partial class PurInvListForm : Form
    {
        public PurInvListForm()
        {
            InitializeComponent();
        }

        private void PurInvListForm_Load(object sender, EventArgs e)
        {
            updateGrid();
        }
        private void updateGrid()
        {
            DataTable piDT = DataAccess.instance.PurchaseInvoice.GetAll();
            invoiceGrid.DataSource = piDT;
            this.invoiceGrid.Columns["Id"].Visible = false;
            //this.invoiceGrid.Columns["ImagePath"].Visible = false;
            //this.invoiceGrid.Columns["CategoryId"].Visible = false;
            this.invoiceGrid.Columns["deleteBtn"].DisplayIndex = Convert.ToInt32(this.invoiceGrid.ColumnCount) - 1;
            loaderPicBox.Hide();
        }

        private void invoiceGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = this.invoiceGrid.Columns[e.ColumnIndex].Name;
            switch (colName)
            {
                case "deleteBtn":
                    MessageBox.Show("delete clicked");
                    break;
            }
        }

        private void newPurBtn_Click(object sender, EventArgs e)
        {
            loaderPicBox.Show();
            loaderPicBox.Hide();
        }
    }
}
