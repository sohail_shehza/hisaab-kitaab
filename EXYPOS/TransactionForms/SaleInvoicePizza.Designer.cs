﻿namespace EXYPOS.TransactionForms
{
    partial class SaleInvoicePizza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaleInvoicePizza));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mopBankCombo = new System.Windows.Forms.ComboBox();
            this.mopCashRadio = new System.Windows.Forms.RadioButton();
            this.mopBankRadio = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.mopCreditRadio = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.discountTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.subTotalTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.descTxt = new System.Windows.Forms.TextBox();
            this.customerCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.invoiceNoTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mobileNoTxt = new System.Windows.Forms.TextBox();
            this.dateCombo = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchProdBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.prodCodeSrTxt = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPhoneNoRequired = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.invTypeCombo = new System.Windows.Forms.ComboBox();
            this.invoiceGrid = new System.Windows.Forms.DataGridView();
            this.pId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchasePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.lblTotalItemsPage = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblPreviousPage = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDue = new System.Windows.Forms.TextBox();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtReceived = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.totalTxt = new System.Windows.Forms.TextBox();
            this.loaderPicBox = new System.Windows.Forms.PictureBox();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mopBankCombo
            // 
            this.mopBankCombo.FormattingEnabled = true;
            this.mopBankCombo.Location = new System.Drawing.Point(4, 33);
            this.mopBankCombo.Name = "mopBankCombo";
            this.mopBankCombo.Size = new System.Drawing.Size(158, 21);
            this.mopBankCombo.TabIndex = 22;
            // 
            // mopCashRadio
            // 
            this.mopCashRadio.AutoSize = true;
            this.mopCashRadio.Checked = true;
            this.mopCashRadio.Location = new System.Drawing.Point(4, 11);
            this.mopCashRadio.Name = "mopCashRadio";
            this.mopCashRadio.Size = new System.Drawing.Size(49, 17);
            this.mopCashRadio.TabIndex = 19;
            this.mopCashRadio.TabStop = true;
            this.mopCashRadio.Text = "Cash";
            this.mopCashRadio.UseVisualStyleBackColor = true;
            // 
            // mopBankRadio
            // 
            this.mopBankRadio.AutoSize = true;
            this.mopBankRadio.Location = new System.Drawing.Point(112, 11);
            this.mopBankRadio.Name = "mopBankRadio";
            this.mopBankRadio.Size = new System.Drawing.Size(50, 17);
            this.mopBankRadio.TabIndex = 21;
            this.mopBankRadio.TabStop = true;
            this.mopBankRadio.Text = "Bank";
            this.mopBankRadio.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.mopBankCombo);
            this.groupBox5.Controls.Add(this.mopCashRadio);
            this.groupBox5.Controls.Add(this.mopBankRadio);
            this.groupBox5.Controls.Add(this.mopCreditRadio);
            this.groupBox5.Location = new System.Drawing.Point(669, 606);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(167, 59);
            this.groupBox5.TabIndex = 23;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "MOP";
            // 
            // mopCreditRadio
            // 
            this.mopCreditRadio.AutoSize = true;
            this.mopCreditRadio.Location = new System.Drawing.Point(55, 11);
            this.mopCreditRadio.Name = "mopCreditRadio";
            this.mopCreditRadio.Size = new System.Drawing.Size(52, 17);
            this.mopCreditRadio.TabIndex = 20;
            this.mopCreditRadio.TabStop = true;
            this.mopCreditRadio.Text = "Credit";
            this.mopCreditRadio.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 624);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Total";
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(704, 666);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(132, 36);
            this.saveBtn.TabIndex = 16;
            this.saveBtn.Text = "Save and Print";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 680);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Discount";
            // 
            // discountTxt
            // 
            this.discountTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.discountTxt.BackColor = System.Drawing.SystemColors.Window;
            this.discountTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountTxt.Location = new System.Drawing.Point(64, 669);
            this.discountTxt.Name = "discountTxt";
            this.discountTxt.Size = new System.Drawing.Size(135, 29);
            this.discountTxt.TabIndex = 9;
            this.discountTxt.Text = "0";
            this.discountTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.discountTxt.TextChanged += new System.EventHandler(this.discountTxt_TextChanged);
            this.discountTxt.Validating += new System.ComponentModel.CancelEventHandler(this.discountTxt_Validating);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 624);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sub Toal";
            // 
            // subTotalTxt
            // 
            this.subTotalTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.subTotalTxt.BackColor = System.Drawing.SystemColors.Control;
            this.subTotalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subTotalTxt.Location = new System.Drawing.Point(64, 613);
            this.subTotalTxt.Name = "subTotalTxt";
            this.subTotalTxt.ReadOnly = true;
            this.subTotalTxt.Size = new System.Drawing.Size(135, 29);
            this.subTotalTxt.TabIndex = 7;
            this.subTotalTxt.Text = "0";
            this.subTotalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Description";
            // 
            // descTxt
            // 
            this.descTxt.Location = new System.Drawing.Point(122, 85);
            this.descTxt.Multiline = true;
            this.descTxt.Name = "descTxt";
            this.descTxt.Size = new System.Drawing.Size(215, 49);
            this.descTxt.TabIndex = 15;
            // 
            // customerCombo
            // 
            this.customerCombo.FormattingEnabled = true;
            this.customerCombo.Location = new System.Drawing.Point(343, 55);
            this.customerCombo.Name = "customerCombo";
            this.customerCombo.Size = new System.Drawing.Size(178, 21);
            this.customerCombo.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(286, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Customer";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(40, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Invoice #";
            // 
            // invoiceNoTxt
            // 
            this.invoiceNoTxt.Location = new System.Drawing.Point(122, 52);
            this.invoiceNoTxt.Name = "invoiceNoTxt";
            this.invoiceNoTxt.ReadOnly = true;
            this.invoiceNoTxt.Size = new System.Drawing.Size(141, 20);
            this.invoiceNoTxt.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(286, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Mobile";
            // 
            // mobileNoTxt
            // 
            this.mobileNoTxt.Location = new System.Drawing.Point(343, 10);
            this.mobileNoTxt.Name = "mobileNoTxt";
            this.mobileNoTxt.Size = new System.Drawing.Size(178, 20);
            this.mobileNoTxt.TabIndex = 18;
            this.mobileNoTxt.TextChanged += new System.EventHandler(this.mobileNoTxt_TextChanged);
            // 
            // dateCombo
            // 
            this.dateCombo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateCombo.Location = new System.Drawing.Point(600, 10);
            this.dateCombo.Name = "dateCombo";
            this.dateCombo.Size = new System.Drawing.Size(144, 20);
            this.dateCombo.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(553, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Date";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.searchProdBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.prodCodeSrTxt);
            this.groupBox1.Location = new System.Drawing.Point(12, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(826, 57);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Product";
            // 
            // searchProdBtn
            // 
            this.searchProdBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.searchProdBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchProdBtn.BackgroundImage")));
            this.searchProdBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchProdBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchProdBtn.Location = new System.Drawing.Point(269, 16);
            this.searchProdBtn.Name = "searchProdBtn";
            this.searchProdBtn.Size = new System.Drawing.Size(30, 20);
            this.searchProdBtn.TabIndex = 16;
            this.searchProdBtn.UseVisualStyleBackColor = false;
            this.searchProdBtn.Click += new System.EventHandler(this.searchProdBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Product Code";
            // 
            // prodCodeSrTxt
            // 
            this.prodCodeSrTxt.HideSelection = false;
            this.prodCodeSrTxt.Location = new System.Drawing.Point(122, 16);
            this.prodCodeSrTxt.Name = "prodCodeSrTxt";
            this.prodCodeSrTxt.Size = new System.Drawing.Size(141, 20);
            this.prodCodeSrTxt.TabIndex = 1;
            this.prodCodeSrTxt.TextChanged += new System.EventHandler(this.prodCodeSrTxt_TextChanged);
            this.prodCodeSrTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.prodCodeSrTxt_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblPhoneNoRequired);
            this.groupBox2.Controls.Add(this.descTxt);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.customerCombo);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.mobileNoTxt);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.dateCombo);
            this.groupBox2.Controls.Add(this.invoiceNoTxt);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.invTypeCombo);
            this.groupBox2.Location = new System.Drawing.Point(12, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(826, 141);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Invoice Type";
            // 
            // lblPhoneNoRequired
            // 
            this.lblPhoneNoRequired.AutoSize = true;
            this.lblPhoneNoRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPhoneNoRequired.Location = new System.Drawing.Point(340, 34);
            this.lblPhoneNoRequired.Name = "lblPhoneNoRequired";
            this.lblPhoneNoRequired.Size = new System.Drawing.Size(104, 13);
            this.lblPhoneNoRequired.TabIndex = 20;
            this.lblPhoneNoRequired.Text = "Phone # is Required";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Invoice Type";
            // 
            // invTypeCombo
            // 
            this.invTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.invTypeCombo.FormattingEnabled = true;
            this.invTypeCombo.Items.AddRange(new object[] {
            "Sale",
            "Sale Return"});
            this.invTypeCombo.Location = new System.Drawing.Point(122, 10);
            this.invTypeCombo.Name = "invTypeCombo";
            this.invTypeCombo.Size = new System.Drawing.Size(141, 21);
            this.invTypeCombo.TabIndex = 14;
            // 
            // invoiceGrid
            // 
            this.invoiceGrid.AllowUserToAddRows = false;
            this.invoiceGrid.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.invoiceGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.invoiceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invoiceGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.invoiceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.invoiceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.invoiceGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pId,
            this.itemBarcode,
            this.itemName,
            this.unit,
            this.salePrice,
            this.purchasePrice,
            this.discount,
            this.quantity,
            this.amount,
            this.stockAvailable});
            this.invoiceGrid.EnableHeadersVisualStyles = false;
            this.invoiceGrid.Location = new System.Drawing.Point(12, 224);
            this.invoiceGrid.Name = "invoiceGrid";
            this.invoiceGrid.RowHeadersVisible = false;
            this.invoiceGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.invoiceGrid.Size = new System.Drawing.Size(826, 379);
            this.invoiceGrid.TabIndex = 22;
            this.invoiceGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.invoiceGrid_CellValueChanged);
            this.invoiceGrid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.invoiceGrid_KeyUp);
            // 
            // pId
            // 
            this.pId.HeaderText = "Id";
            this.pId.Name = "pId";
            this.pId.Visible = false;
            // 
            // itemBarcode
            // 
            this.itemBarcode.HeaderText = "Item/Barcode";
            this.itemBarcode.Name = "itemBarcode";
            this.itemBarcode.ReadOnly = true;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Item Name";
            this.itemName.Name = "itemName";
            this.itemName.ReadOnly = true;
            // 
            // unit
            // 
            this.unit.HeaderText = "Unit";
            this.unit.Name = "unit";
            this.unit.ReadOnly = true;
            // 
            // salePrice
            // 
            this.salePrice.HeaderText = "Sale Price";
            this.salePrice.Name = "salePrice";
            this.salePrice.ReadOnly = true;
            // 
            // purchasePrice
            // 
            this.purchasePrice.HeaderText = "Purchase Price";
            this.purchasePrice.Name = "purchasePrice";
            // 
            // discount
            // 
            this.discount.HeaderText = "Discount";
            this.discount.Name = "discount";
            // 
            // quantity
            // 
            this.quantity.HeaderText = "Quantity";
            this.quantity.Name = "quantity";
            // 
            // amount
            // 
            this.amount.HeaderText = "Amount";
            this.amount.Name = "amount";
            this.amount.ReadOnly = true;
            // 
            // stockAvailable
            // 
            this.stockAvailable.HeaderText = "StockAvailable";
            this.stockAvailable.Name = "stockAvailable";
            this.stockAvailable.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 727);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1370, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(995, 674);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 29;
            this.button2.Text = "Prev";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(1174, 673);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 30;
            this.button3.Text = "Next";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1080, 679);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Page";
            // 
            // lblTotalItemsPage
            // 
            this.lblTotalItemsPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalItemsPage.AutoSize = true;
            this.lblTotalItemsPage.Location = new System.Drawing.Point(1152, 679);
            this.lblTotalItemsPage.Name = "lblTotalItemsPage";
            this.lblTotalItemsPage.Size = new System.Drawing.Size(13, 13);
            this.lblTotalItemsPage.TabIndex = 32;
            this.lblTotalItemsPage.Text = "1";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1133, 679);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "of";
            // 
            // lblPreviousPage
            // 
            this.lblPreviousPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreviousPage.AutoSize = true;
            this.lblPreviousPage.Location = new System.Drawing.Point(1114, 680);
            this.lblPreviousPage.Name = "lblPreviousPage";
            this.lblPreviousPage.Size = new System.Drawing.Size(13, 13);
            this.lblPreviousPage.TabIndex = 34;
            this.lblPreviousPage.Text = "1";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(864, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Categories Filter";
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Items.AddRange(new object[] {
            "Sale",
            "Sale Return"});
            this.categoryComboBox.Location = new System.Drawing.Point(952, 12);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(141, 21);
            this.categoryComboBox.TabIndex = 35;
            this.categoryComboBox.SelectedIndexChanged += new System.EventHandler(this.categoryComboBox_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(425, 624);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "Amount Due";
            // 
            // txtDue
            // 
            this.txtDue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDue.BackColor = System.Drawing.SystemColors.Control;
            this.txtDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDue.Location = new System.Drawing.Point(508, 613);
            this.txtDue.Name = "txtDue";
            this.txtDue.ReadOnly = true;
            this.txtDue.Size = new System.Drawing.Size(142, 29);
            this.txtDue.TabIndex = 39;
            this.txtDue.Text = "0";
            this.txtDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBalance
            // 
            this.txtBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalance.Location = new System.Drawing.Point(508, 669);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(142, 29);
            this.txtBalance.TabIndex = 41;
            this.txtBalance.Text = "0";
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtBalance.TextChanged += new System.EventHandler(this.txtPay_TextChanged);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(425, 680);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Balance Due";
            // 
            // txtReceived
            // 
            this.txtReceived.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReceived.BackColor = System.Drawing.SystemColors.Window;
            this.txtReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceived.Location = new System.Drawing.Point(267, 670);
            this.txtReceived.Name = "txtReceived";
            this.txtReceived.Size = new System.Drawing.Size(143, 29);
            this.txtReceived.TabIndex = 43;
            this.txtReceived.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtReceived.TextChanged += new System.EventHandler(this.txtReceived_TextChanged);
            this.txtReceived.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReceived_KeyDown);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(210, 678);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "Received";
            // 
            // totalTxt
            // 
            this.totalTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.totalTxt.Location = new System.Drawing.Point(267, 613);
            this.totalTxt.Multiline = true;
            this.totalTxt.Name = "totalTxt";
            this.totalTxt.ReadOnly = true;
            this.totalTxt.Size = new System.Drawing.Size(143, 29);
            this.totalTxt.TabIndex = 44;
            // 
            // loaderPicBox
            // 
            this.loaderPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loaderPicBox.Image = ((System.Drawing.Image)(resources.GetObject("loaderPicBox.Image")));
            this.loaderPicBox.Location = new System.Drawing.Point(1259, 6);
            this.loaderPicBox.Name = "loaderPicBox";
            this.loaderPicBox.Size = new System.Drawing.Size(80, 58);
            this.loaderPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loaderPicBox.TabIndex = 37;
            this.loaderPicBox.TabStop = false;
            // 
            // SaleInvoicePizza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.totalTxt);
            this.Controls.Add(this.txtReceived);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtDue);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.loaderPicBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.categoryComboBox);
            this.Controls.Add(this.lblPreviousPage);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblTotalItemsPage);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.invoiceGrid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.subTotalTxt);
            this.Controls.Add(this.discountTxt);
            this.Controls.Add(this.label6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SaleInvoicePizza";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SaleInvoicePizza";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SaleInvoiceForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SaleInvoiceForm_KeyUp);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchProdBtn;
        private System.Windows.Forms.ComboBox mopBankCombo;
        private System.Windows.Forms.RadioButton mopCashRadio;
        private System.Windows.Forms.RadioButton mopBankRadio;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton mopCreditRadio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox discountTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox subTotalTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox descTxt;
        private System.Windows.Forms.ComboBox customerCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox invoiceNoTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox prodCodeSrTxt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox invTypeCombo;
        private System.Windows.Forms.DataGridView invoiceGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn pId;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn salePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchasePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockAvailable;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox mobileNoTxt;
        private System.Windows.Forms.Label lblPhoneNoRequired;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblTotalItemsPage;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblPreviousPage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.PictureBox loaderPicBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDue;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtReceived;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox totalTxt;
    }
}