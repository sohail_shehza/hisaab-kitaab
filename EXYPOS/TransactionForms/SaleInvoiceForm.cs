﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.TransactionForms
{
    public partial class SaleInvoiceForm : Form
    {
  
        //private Boolean discountInPrecent = false;
        public SaleInvoiceForm()
        {
            InitializeComponent();
        }


        private void searchProdBtn_Click(object sender, EventArgs e)
        {
            ProductListForm pf = new ProductListForm(true);
            pf.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            pf.refForm = this;
            pf.ShowDialog(this);
        }

        public void handleSearch(DataGridViewRow row)
        {
            try
            {
                int StockAvail = Convert.ToInt32(row.Cells["StockAvailable"].Value);
                if (StockAvail > 0)
                {
                    invoiceGrid.Rows.Add(row.Cells["Id"].Value, row.Cells["Barcode"].Value, row.Cells["Title"].Value, row.Cells["Unit"].Value, row.Cells["SalePrice"].Value, row.Cells["PurchasePrice"].Value, 0, 1, Convert.ToInt32(row.Cells["SalePrice"].Value) * 1, StockAvail);
                    invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                    calcInvoice();
                }
            }
            catch (Exception ex) { }
        }
        private void prodCodeSrTxt_TextChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(prodCodeSrTxt.Text);
        }

        private void updateGrid()
        {
            invTypeCombo.SelectedItem = "Sale";
            //DataTable csDT = DataAccess.instance.Customers.GetAll();
            DataTable csDT = DataAccess.instance.Accounts.GetCustomerAccounts();
            customerCombo.DisplayMember = "Title";
            customerCombo.ValueMember = "Id";
            customerCombo.DataSource = csDT;
            customerCombo.SelectedValue = 3;
            invoiceGrid.Focus();

            DataTable baDT = DataAccess.instance.Accounts.GetAllBankAccounts();
            mopBankCombo.DisplayMember = "Title";
            mopBankCombo.ValueMember = "Id";
            mopBankCombo.DataSource = baDT;
            invoiceGrid.Focus();

            //invoiceNoTxt.Text = DataAccess.instance.Utility.autoNextNumber("InvoiceNo").ToString();
        }

        private void invoiceGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (invoiceGrid.CurrentCell != null)
                {
                    DataGridViewRow invRow = (DataGridViewRow)invoiceGrid.Rows[e.RowIndex];
                    var currentCell = invoiceGrid.CurrentCell;
                    var columnName = invoiceGrid.Columns[e.ColumnIndex].Name;
                    int qty = Convert.ToInt32(invoiceGrid.CurrentRow.Cells["quantity"].Value);
                    decimal purPrice = Convert.ToDecimal(invoiceGrid.CurrentRow.Cells["salePrice"].Value);
                    decimal rowSubTotal = purPrice * qty;
                    string discount = invoiceGrid.CurrentRow.Cells["discount"].Value.ToString();
                    int stockAvail = Convert.ToInt32(invoiceGrid.CurrentRow.Cells["stockAvailable"].Value);
                    if (qty > stockAvail)
                    {
                        MessageBox.Show("Exceeded Maximmum Stock");
                        invoiceGrid.CurrentRow.Cells["quantity"].Value = stockAvail;
                        qty = stockAvail;
                    }
                    if (discount.IndexOf("%") > -1)
                    {
                        char[] MyChar = { '%', ' ' };
                        decimal dis = Convert.ToDecimal(discount.Trim(MyChar));
                        invoiceGrid.CurrentRow.Cells["discount"].Value = rowSubTotal * dis / 100;
                    }

                    invoiceGrid.CurrentRow.Cells["amount"].Value = rowSubTotal - Convert.ToDecimal(invoiceGrid.CurrentRow.Cells["discount"].Value);
                    calcInvoice();
                    // subTotal.Text = invoiceGrid.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells["amount"].Value)).ToString();
                    //calcDiscount();
                }
            }
            catch (Exception ex) { }
        }

        private void SaleInvoiceForm_Load(object sender, EventArgs e)
        {
            mopBankCombo.Enabled = false;
            updateGrid();
        }

        private void SaleInvoiceForm_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void invoiceGrid_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.F12)
            //{
            //    this.discountInPrecent = !this.discountInPrecent;
            //    if(this.discountInPrecent)
            //    {
            //        this.discountStatus.Text = "Discount Type : Percentage";
            //    }
            //    else
            //    {
            //        this.discountStatus.Text = "Discount Type : Price";
            //    }
            //}
        }
        private void calcInvoice()
        {
            try
            {
                subTotalTxt.Text = invoiceGrid.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells["amount"].Value)).ToString();
                totalTxt.Text = (Convert.ToDecimal(subTotalTxt.Text)  - Convert.ToDecimal(discountTxt.Text)).ToString();
            }
            catch (Exception ex) { }
        }
        private void prodCodeSrTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string itemCode = prodCodeSrTxt.Text;
                DataTable itemDT = DataAccess.instance.Products.GetByItemCode(itemCode);
                if (itemDT.Rows.Count > 0)
                {
                    int Id = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("Id"));
                    string Barcode = itemDT.Rows[0].Field<String>("Barcode");
                    String Title = itemDT.Rows[0].Field<String>("Title");
                    String Unit = itemDT.Rows[0].Field<String>("Unit");
                    decimal PurchasePrice = itemDT.Rows[0].Field<decimal>("PurchasePrice");
                    decimal SalePrice = itemDT.Rows[0].Field<decimal>("SalePrice");
                    int stockAvail = Convert.ToInt32(itemDT.Rows[0].Field<dynamic>("StockAvailable"));
                    if (stockAvail > 0)
                    {
                        invoiceGrid.Rows.Add(Id, Barcode, Title, Unit, SalePrice,PurchasePrice,0, 1, SalePrice * 1);
                        invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["quantity"];
                        calcInvoice();
                    }
                    else
                    {
                        MessageBox.Show("Stock not available");
                    }
                }
            }
        }
        private Boolean validateForm()
        {
            string invType = invTypeCombo.Text.Trim();
            if (invType == "")
            {
                MessageBox.Show("Select Invoice Type");
                return false;
            }

            //if(this.invNoTxt.Text.Trim()== "")
            //{
            //    MessageBox.Show("Enter a valid Invoice#");
            //    return false;
            //}
            if (!DateTime.TryParse(this.dateCombo.Text, out var tempDate))
            {
                MessageBox.Show("Enter a valid Date");
                return false;
            }
            int supplierAccId = Convert.ToInt32(this.customerCombo.SelectedValue);
            if (supplierAccId <= 0)
            {
                MessageBox.Show("Please Select customer");
                return false;
            }

            Boolean mopBank = mopBankRadio.Checked;
            if (mopBank)
            {

                int bankId = Convert.ToInt32(mopBankCombo.SelectedValue);
                if (bankId <= 0)
                {
                    MessageBox.Show("Select a bank");
                    return false;
                }
            }
            if (invoiceGrid.Rows.Count <= 0)
            {
                MessageBox.Show("You can't save a zero item invoice");
                return false;
            }
            return true;
        }
        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateForm())
                {
                    calcInvoice();
                    string invNo = this.invoiceNoTxt.Text.Trim();
                    DateTime date = this.dateCombo.Value;
                    int customerId = Convert.ToInt32(this.customerCombo.SelectedValue);
                    string desc = this.descTxt.Text.Trim();
                    string FinalDis = this.discountTxt.Text.Trim();
                    decimal subTotal = Convert.ToDecimal(this.subTotalTxt.Text);
                    decimal total = Convert.ToDecimal(this.totalTxt.Text);
                    string invType = invTypeCombo.Text.Trim();
                    if (invType == "Sale")
                    {
                        int saleInv = DataAccess.instance.SaleInvoice.insert(invNo, customerId, date, desc, FinalDis, subTotal, total,"",0,0);
                        if (saleInv > 0)
                        {
                            foreach (DataGridViewRow row in invoiceGrid.Rows)
                            {
                                int pId = Convert.ToInt32(row.Cells["pId"].Value);
                                if (pId > 0)
                                {
                                    string itemName = row.Cells["itemName"].Value.ToString();
                                    string unit = row.Cells["unit"].Value.ToString();
                                    decimal purchasePrice = Convert.ToDecimal(row.Cells["purchasePrice"].Value);
                                    int quantity = Convert.ToInt32(row.Cells["quantity"].Value);
                                    decimal unitPr = Convert.ToDecimal(row.Cells["salePrice"].Value);
                                    DataAccess.instance.SaleInvoice.insertDetails(saleInv, pId, purchasePrice, quantity, unitPr);
                                }
                            }
                            //create sale type voucher
                            int vId = DataAccess.instance.Voucher.insert(0, 3, saleInv, date, "Item Sold");
                            Boolean mopCash = mopCashRadio.Checked;
                            Boolean mopCredit = mopCreditRadio.Checked;
                            Boolean mopBank = mopBankRadio.Checked;
                            if (mopCash)
                            {
                                //Debit sale to cash account
                                DataAccess.instance.Voucher.insertDetails(vId, 2, total, 0, "Debit against sale");
                            }
                            else if (mopCredit)
                            {
                                //Debit sale bill to customer
                                DataAccess.instance.Voucher.insertDetails(vId, customerId, total, 0, "Debit against sale");

                            }
                            else if (mopBank)
                            {
                                int bankId = Convert.ToInt32(mopBankCombo.SelectedValue);
                                //Debit sale bill to bank
                                DataAccess.instance.Voucher.insertDetails(vId, bankId, total, 0, "Debit against sale");

                            }
                            //get sale account by code
                            DataTable ivAcc = DataAccess.instance.Accounts.GetAccByCode("SALE-001");
                            int ivAccId = Convert.ToInt32(ivAcc.Rows[0].Field<dynamic>("Id"));
                            //credit sale account
                            DataAccess.instance.Voucher.insertDetails(vId, ivAccId, 0, subTotal, "credit against sale");
                            int fDis = Convert.ToInt32(FinalDis);
                            if (fDis > 0)
                            {
                                //if discount then debit to discount accouunt
                                DataTable disAcc = DataAccess.instance.Accounts.GetAccByCode("DISCOUNT-001");
                                int disAccId = Convert.ToInt32(ivAcc.Rows[0].Field<dynamic>("Id"));
                                DataAccess.instance.Voucher.insertDetails(vId, disAccId, fDis, 0, "debit discount against sale");
                            }
                            MessageBox.Show("Invoice Created");
                        }
                    }
                }
            }
            catch (Exception ex) { }
            
        }

        private void mopBankRadio_CheckedChanged(object sender, EventArgs e)
        {
            Boolean mopBank = mopBankRadio.Checked;
            if(mopBank)
            {
                mopBankCombo.Enabled = true;
            }
            else
            {
                mopBankCombo.Enabled = false;
            }
        }

        private void discountTxt_Validating(object sender, CancelEventArgs e)
        {
            string discount = discountTxt.Text.Trim();
            if (discount.IndexOf("%") > -1)
            {
                char[] MyChar = { '%', ' ' };
                decimal dis = Convert.ToDecimal(discount.Trim(MyChar));
                discountTxt.Text = (Convert.ToDecimal(subTotalTxt.Text) * dis / 100).ToString();
            }
            if (discount == "")
            {
                discountTxt.Text = "0";
            }
            calcInvoice();
        }
    }
}
