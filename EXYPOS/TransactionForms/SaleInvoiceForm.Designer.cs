﻿namespace EXYPOS.TransactionForms
{
    partial class SaleInvoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaleInvoiceForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchProdBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.prodCodeSrTxt = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.loaderPicBox = new System.Windows.Forms.PictureBox();
            this.invTypeCombo = new System.Windows.Forms.ComboBox();
            this.invoiceGrid = new System.Windows.Forms.DataGridView();
            this.pId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchasePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.descTxt = new System.Windows.Forms.TextBox();
            this.customerCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateCombo = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.invoiceNoTxt = new System.Windows.Forms.TextBox();
            this.subTotalTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.discountTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.totalTxt = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.mopBankCombo = new System.Windows.Forms.ComboBox();
            this.mopCashRadio = new System.Windows.Forms.RadioButton();
            this.mopBankRadio = new System.Windows.Forms.RadioButton();
            this.mopCreditRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.searchProdBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.prodCodeSrTxt);
            this.groupBox1.Location = new System.Drawing.Point(12, 218);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(790, 69);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Product";
            // 
            // searchProdBtn
            // 
            this.searchProdBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.searchProdBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchProdBtn.BackgroundImage")));
            this.searchProdBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchProdBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchProdBtn.Location = new System.Drawing.Point(323, 24);
            this.searchProdBtn.Name = "searchProdBtn";
            this.searchProdBtn.Size = new System.Drawing.Size(30, 30);
            this.searchProdBtn.TabIndex = 16;
            this.searchProdBtn.UseVisualStyleBackColor = false;
            this.searchProdBtn.Click += new System.EventHandler(this.searchProdBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Product Code";
            // 
            // prodCodeSrTxt
            // 
            this.prodCodeSrTxt.Location = new System.Drawing.Point(95, 29);
            this.prodCodeSrTxt.Name = "prodCodeSrTxt";
            this.prodCodeSrTxt.Size = new System.Drawing.Size(222, 20);
            this.prodCodeSrTxt.TabIndex = 7;
            this.prodCodeSrTxt.TextChanged += new System.EventHandler(this.prodCodeSrTxt_TextChanged);
            this.prodCodeSrTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.prodCodeSrTxt_KeyUp);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.loaderPicBox);
            this.groupBox2.Controls.Add(this.invTypeCombo);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(790, 64);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Invoice Type";
            // 
            // loaderPicBox
            // 
            this.loaderPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loaderPicBox.Image = ((System.Drawing.Image)(resources.GetObject("loaderPicBox.Image")));
            this.loaderPicBox.Location = new System.Drawing.Point(704, 7);
            this.loaderPicBox.Name = "loaderPicBox";
            this.loaderPicBox.Size = new System.Drawing.Size(80, 52);
            this.loaderPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loaderPicBox.TabIndex = 17;
            this.loaderPicBox.TabStop = false;
            // 
            // invTypeCombo
            // 
            this.invTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.invTypeCombo.FormattingEnabled = true;
            this.invTypeCombo.Items.AddRange(new object[] {
            "Sale",
            "Sale Return"});
            this.invTypeCombo.Location = new System.Drawing.Point(23, 29);
            this.invTypeCombo.Name = "invTypeCombo";
            this.invTypeCombo.Size = new System.Drawing.Size(121, 21);
            this.invTypeCombo.TabIndex = 14;
            // 
            // invoiceGrid
            // 
            this.invoiceGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.invoiceGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.invoiceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invoiceGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.invoiceGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.invoiceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.invoiceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.invoiceGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pId,
            this.itemBarcode,
            this.itemName,
            this.unit,
            this.salePrice,
            this.purchasePrice,
            this.discount,
            this.quantity,
            this.amount,
            this.stockAvailable});
            this.invoiceGrid.EnableHeadersVisualStyles = false;
            this.invoiceGrid.Location = new System.Drawing.Point(12, 299);
            this.invoiceGrid.Name = "invoiceGrid";
            this.invoiceGrid.RowHeadersVisible = false;
            this.invoiceGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.invoiceGrid.Size = new System.Drawing.Size(790, 130);
            this.invoiceGrid.TabIndex = 15;
            this.invoiceGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.invoiceGrid_CellValueChanged);
            this.invoiceGrid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.invoiceGrid_KeyUp);
            // 
            // pId
            // 
            this.pId.HeaderText = "Id";
            this.pId.Name = "pId";
            this.pId.Visible = false;
            // 
            // itemBarcode
            // 
            this.itemBarcode.HeaderText = "Item/Barcode";
            this.itemBarcode.Name = "itemBarcode";
            this.itemBarcode.ReadOnly = true;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Item Name";
            this.itemName.Name = "itemName";
            this.itemName.ReadOnly = true;
            // 
            // unit
            // 
            this.unit.HeaderText = "Unit";
            this.unit.Name = "unit";
            this.unit.ReadOnly = true;
            // 
            // salePrice
            // 
            this.salePrice.HeaderText = "Sale Price";
            this.salePrice.Name = "salePrice";
            this.salePrice.ReadOnly = true;
            // 
            // purchasePrice
            // 
            this.purchasePrice.HeaderText = "Purchase Price";
            this.purchasePrice.Name = "purchasePrice";
            // 
            // discount
            // 
            this.discount.HeaderText = "Discount";
            this.discount.Name = "discount";
            // 
            // quantity
            // 
            this.quantity.HeaderText = "Quantity";
            this.quantity.Name = "quantity";
            // 
            // amount
            // 
            this.amount.HeaderText = "Amount";
            this.amount.Name = "amount";
            this.amount.ReadOnly = true;
            // 
            // stockAvailable
            // 
            this.stockAvailable.HeaderText = "StockAvailable";
            this.stockAvailable.Name = "stockAvailable";
            this.stockAvailable.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 455);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1086, 22);
            this.statusStrip1.TabIndex = 20;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.descTxt);
            this.groupBox4.Controls.Add(this.customerCombo);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.dateCombo);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.invoiceNoTxt);
            this.groupBox4.Location = new System.Drawing.Point(12, 82);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(790, 130);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Invoice Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Description";
            // 
            // descTxt
            // 
            this.descTxt.Location = new System.Drawing.Point(95, 65);
            this.descTxt.Multiline = true;
            this.descTxt.Name = "descTxt";
            this.descTxt.Size = new System.Drawing.Size(305, 52);
            this.descTxt.TabIndex = 15;
            // 
            // customerCombo
            // 
            this.customerCombo.FormattingEnabled = true;
            this.customerCombo.Location = new System.Drawing.Point(518, 29);
            this.customerCombo.Name = "customerCombo";
            this.customerCombo.Size = new System.Drawing.Size(178, 21);
            this.customerCombo.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(457, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Customer";
            // 
            // dateCombo
            // 
            this.dateCombo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateCombo.Location = new System.Drawing.Point(297, 29);
            this.dateCombo.Name = "dateCombo";
            this.dateCombo.Size = new System.Drawing.Size(103, 20);
            this.dateCombo.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(242, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Invoice #";
            // 
            // invoiceNoTxt
            // 
            this.invoiceNoTxt.Location = new System.Drawing.Point(95, 29);
            this.invoiceNoTxt.Name = "invoiceNoTxt";
            this.invoiceNoTxt.Size = new System.Drawing.Size(100, 20);
            this.invoiceNoTxt.TabIndex = 7;
            // 
            // subTotalTxt
            // 
            this.subTotalTxt.BackColor = System.Drawing.SystemColors.Control;
            this.subTotalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subTotalTxt.Location = new System.Drawing.Point(23, 48);
            this.subTotalTxt.Name = "subTotalTxt";
            this.subTotalTxt.ReadOnly = true;
            this.subTotalTxt.Size = new System.Drawing.Size(185, 29);
            this.subTotalTxt.TabIndex = 7;
            this.subTotalTxt.Text = "0";
            this.subTotalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sub Toal";
            // 
            // discountTxt
            // 
            this.discountTxt.BackColor = System.Drawing.SystemColors.Window;
            this.discountTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountTxt.Location = new System.Drawing.Point(23, 115);
            this.discountTxt.Name = "discountTxt";
            this.discountTxt.Size = new System.Drawing.Size(185, 29);
            this.discountTxt.TabIndex = 9;
            this.discountTxt.Text = "0";
            this.discountTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.discountTxt.Validating += new System.ComponentModel.CancelEventHandler(this.discountTxt_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Discount";
            // 
            // totalTxt
            // 
            this.totalTxt.BackColor = System.Drawing.Color.MidnightBlue;
            this.totalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTxt.ForeColor = System.Drawing.SystemColors.Window;
            this.totalTxt.Location = new System.Drawing.Point(23, 171);
            this.totalTxt.Name = "totalTxt";
            this.totalTxt.ReadOnly = true;
            this.totalTxt.Size = new System.Drawing.Size(185, 29);
            this.totalTxt.TabIndex = 10;
            this.totalTxt.Text = "0";
            this.totalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // saveBtn
            // 
            this.saveBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(102, 338);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(121, 35);
            this.saveBtn.TabIndex = 16;
            this.saveBtn.Text = "Save and Print";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Total";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.saveBtn);
            this.groupBox3.Controls.Add(this.totalTxt);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.discountTxt);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.subTotalTxt);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Location = new System.Drawing.Point(826, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 417);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Payment Summary";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.mopBankCombo);
            this.groupBox5.Controls.Add(this.mopCashRadio);
            this.groupBox5.Controls.Add(this.mopBankRadio);
            this.groupBox5.Controls.Add(this.mopCreditRadio);
            this.groupBox5.Location = new System.Drawing.Point(23, 218);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 100);
            this.groupBox5.TabIndex = 23;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "MOP";
            // 
            // mopBankCombo
            // 
            this.mopBankCombo.FormattingEnabled = true;
            this.mopBankCombo.Location = new System.Drawing.Point(9, 57);
            this.mopBankCombo.Name = "mopBankCombo";
            this.mopBankCombo.Size = new System.Drawing.Size(176, 21);
            this.mopBankCombo.TabIndex = 22;
            // 
            // mopCashRadio
            // 
            this.mopCashRadio.AutoSize = true;
            this.mopCashRadio.Checked = true;
            this.mopCashRadio.Location = new System.Drawing.Point(9, 25);
            this.mopCashRadio.Name = "mopCashRadio";
            this.mopCashRadio.Size = new System.Drawing.Size(49, 17);
            this.mopCashRadio.TabIndex = 19;
            this.mopCashRadio.TabStop = true;
            this.mopCashRadio.Text = "Cash";
            this.mopCashRadio.UseVisualStyleBackColor = true;
            // 
            // mopBankRadio
            // 
            this.mopBankRadio.AutoSize = true;
            this.mopBankRadio.Location = new System.Drawing.Point(135, 25);
            this.mopBankRadio.Name = "mopBankRadio";
            this.mopBankRadio.Size = new System.Drawing.Size(50, 17);
            this.mopBankRadio.TabIndex = 21;
            this.mopBankRadio.TabStop = true;
            this.mopBankRadio.Text = "Bank";
            this.mopBankRadio.UseVisualStyleBackColor = true;
            this.mopBankRadio.CheckedChanged += new System.EventHandler(this.mopBankRadio_CheckedChanged);
            // 
            // mopCreditRadio
            // 
            this.mopCreditRadio.AutoSize = true;
            this.mopCreditRadio.Location = new System.Drawing.Point(64, 25);
            this.mopCreditRadio.Name = "mopCreditRadio";
            this.mopCreditRadio.Size = new System.Drawing.Size(52, 17);
            this.mopCreditRadio.TabIndex = 20;
            this.mopCreditRadio.TabStop = true;
            this.mopCreditRadio.Text = "Credit";
            this.mopCreditRadio.UseVisualStyleBackColor = true;
            // 
            // SaleInvoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1086, 477);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.invoiceGrid);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaleInvoiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Sale Invoice";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SaleInvoiceForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SaleInvoiceForm_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox prodCodeSrTxt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView invoiceGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button searchProdBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox descTxt;
        private System.Windows.Forms.ComboBox customerCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox invoiceNoTxt;
        private System.Windows.Forms.ComboBox invTypeCombo;
        private System.Windows.Forms.TextBox subTotalTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox discountTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox totalTxt;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox mopBankCombo;
        private System.Windows.Forms.RadioButton mopBankRadio;
        private System.Windows.Forms.RadioButton mopCreditRadio;
        private System.Windows.Forms.RadioButton mopCashRadio;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridViewTextBoxColumn pId;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn salePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchasePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockAvailable;
        private System.Windows.Forms.PictureBox loaderPicBox;
    }
}