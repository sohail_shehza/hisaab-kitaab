﻿using DAL;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.ReportForms
{
    public partial class TrialBalanceForm : Form
    {
        public TrialBalanceForm()
        {
            InitializeComponent();
        }

        private void TrialBalanceForm_Load(object sender, EventArgs e)
        {
            DataTable tbDT = DataAccess.instance.TrialBalance.Get();
            TrialBalance tbr = new TrialBalance();
            tbr.SetDataSource(tbDT);
            tbrViewer.ReportSource = tbr;
        }
    }
}
