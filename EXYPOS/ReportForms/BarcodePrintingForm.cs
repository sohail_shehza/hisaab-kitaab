﻿using DAL;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.ReportForms
{
    public partial class BarcodePrintingForm : Form
    {
        public BarcodePrintingForm()
        {
            InitializeComponent();
        }

        private void BarcodePrintingForm_Load(object sender, EventArgs e)
        {
            DataTable pDT = DataAccess.instance.Products.GetAll();
            BarcodeBulkPrint barcodeP = new BarcodeBulkPrint();
            barcodeP.SetDataSource(pDT);
            reportViewer.ReportSource = barcodeP;
        }
    }
}
