﻿using DAL;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.ReportForms
{
    public partial class AllReporting : Form
    {
        public AllReporting()
        {
            InitializeComponent();
        }

        private void AllReporting_Load(object sender, EventArgs e)
        {
            GetAllProductCategories();
          
            lblError.Hide();
            loaderPicBox.Hide();
            treeView1.NodeMouseClick +=
                new TreeNodeMouseClickEventHandler(treeView1_NodeMouseClick);

            TreeNode treeNode = new TreeNode("Trial Balance");
            treeView1.Nodes.Add(treeNode);
            treeNode = new TreeNode("Current Stock");
            treeView1.Nodes.Add(treeNode);
            treeNode = new TreeNode("Print Barcode");
            treeView1.Nodes.Add(treeNode);
            TreeNode node2 = new TreeNode("Daily Sale");
            //TreeNode node3 = new TreeNode("Sale By Product");
           // TreeNode[] array = new TreeNode[] { node2, node3 };
            TreeNode[] array = new TreeNode[] { node2 };
           
            treeNode = new TreeNode("Sale Report", array);
            treeView1.Nodes.Add(treeNode);
        }
        private void GetAllProductCategories()
        {
            DataTable dt = DataAccess.instance.Products.GetAllItemName();
            DataRow dr;
            dr = dt.NewRow();
            dr.ItemArray = new object[] { "--Show Product--" };
            dt.Rows.InsertAt(dr, 0);

            categoryComboBox.ValueMember = "Barcode";

            categoryComboBox.DisplayMember = "Title";
            categoryComboBox.DataSource = dt;


        }
       
        void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            lblError.Hide();
            loaderPicBox.Show();
            switch (e.Node.Text)
            {
                case "Trial Balance":
                    TrialBalanceForm tbf = new TrialBalanceForm();
                    tbf.Show(this);
                    // saleInvRepoView.ReportSource = null;
                    //DataTable tbDT = DataAccess.instance.TrialBalance.Get();
                    //TrialBalance tbr = new TrialBalance();
                    //tbr.SetDataSource(tbDT);
                    //saleInvRepoView.ReportSource = tbDT;
                    //saleInvRepoView.Zoom(1);
                    loaderPicBox.Hide();
                    break;
                case "Current Stock":
                    CurrentStockForm csf = new CurrentStockForm();
                    csf.Show(this);
                    loaderPicBox.Hide();
                    break;
                case "Print Barcode":
                    BarcodePrintingForm bpf = new BarcodePrintingForm();
                    bpf.Show(this);
                    loaderPicBox.Hide();
                    break;
                case "Sale Report":
                    lblError.Hide();
                    loaderPicBox.Hide();
                    break;
                default:
                    lblError.Show();
                    loaderPicBox.Hide();
                    break;

            }
        }

        private void categoryComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
           
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int itemBarcode = Convert.ToInt32(categoryComboBox.SelectedValue);
            try
            {
                loaderPicBox.Show();
                //int invId = Convert.ToInt32(recentOrderGrid.Rows[e.RowIndex].Cells["Id"].Value);
                DataTable recentOrderDt = DataAccess.instance.SaleInvoice.GetReportInvByItemBarcode(itemBarcode);
                LoadReport(recentOrderDt);
            }
            catch (Exception ex)
            {
                loaderPicBox.Hide();
            }
        }
        private void  LoadReport(DataTable dt)
        {
            SaleInvoiceReporting sinvs = new SaleInvoiceReporting();
            sinvs.SetDataSource(dt);
            saleInvRepoView.ReportSource = sinvs;
            saleInvRepoView.Zoom(1);
            loaderPicBox.Hide();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                loaderPicBox.Show();
                var start = dpStartDate.Value;
                var end = dpEndDate.Value;
                DataTable recentOrderDt = DataAccess.instance.SaleInvoice.GetReportInvByItemDate(start,end);
                LoadReport(recentOrderDt);
            }
            catch (Exception ex)
            {
                loaderPicBox.Hide();
            }
        }
    }
}
