﻿using DAL;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.ReportForms
{
    public partial class CurrentStockForm : Form
    {
        public CurrentStockForm()
        {
            InitializeComponent();
        }

        private void CurrentStockForm_Load(object sender, EventArgs e)
        {
            DataTable csDT = DataAccess.instance.Products.GetCurrentStock();
            CurrentStock csr = new CurrentStock();
            csr.SetDataSource(csDT);
            csrView.ReportSource = csr;
        }
    }
}
