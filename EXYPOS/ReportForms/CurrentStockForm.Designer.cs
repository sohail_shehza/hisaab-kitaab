﻿namespace EXYPOS.ReportForms
{
    partial class CurrentStockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CurrentStockForm));
            this.csrView = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // csrView
            // 
            this.csrView.ActiveViewIndex = -1;
            this.csrView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.csrView.Cursor = System.Windows.Forms.Cursors.Default;
            this.csrView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.csrView.Location = new System.Drawing.Point(0, 0);
            this.csrView.Name = "csrView";
            this.csrView.ShowCloseButton = false;
            this.csrView.ShowLogo = false;
            this.csrView.Size = new System.Drawing.Size(800, 450);
            this.csrView.TabIndex = 0;
            this.csrView.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // CurrentStockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.csrView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CurrentStockForm";
            this.Text = "CurrentStockForm";
            this.Load += new System.EventHandler(this.CurrentStockForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer csrView;
    }
}