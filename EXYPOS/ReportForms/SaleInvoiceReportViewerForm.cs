﻿using DAL;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.ReportForms
{
    public partial class SaleInvoiceReportViewerForm : Form
    {
        private int saleInv = 0;
        public SaleInvoiceReportViewerForm(int inv = 0)
        {
            InitializeComponent();
            saleInv = inv;
        }

        private void SaleInvoiceReportViewerForm_Load(object sender, EventArgs e)
        {
            
            loaderPicBox.Show();
            DataTable recentOrderDt = DataAccess.instance.SaleInvoice.GetInvById(saleInv);
            SaleInvoicePizzaReport1 sinvs = new SaleInvoicePizzaReport1();
            sinvs.SetDataSource(recentOrderDt);
            saleInvRepoView.ReportSource = sinvs;
            saleInvRepoView.Zoom(1);
            loaderPicBox.Hide();
        }
    }
}
