﻿namespace EXYPOS.ReportForms
{
    partial class TrialBalanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrialBalanceForm));
            this.tbrViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // tbrViewer
            // 
            this.tbrViewer.ActiveViewIndex = -1;
            this.tbrViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbrViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbrViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbrViewer.Location = new System.Drawing.Point(0, 0);
            this.tbrViewer.Name = "tbrViewer";
            this.tbrViewer.ShowCloseButton = false;
            this.tbrViewer.ShowLogo = false;
            this.tbrViewer.Size = new System.Drawing.Size(800, 450);
            this.tbrViewer.TabIndex = 0;
            this.tbrViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // TrialBalanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbrViewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TrialBalanceForm";
            this.Text = "TrialBalanceForm";
            this.Load += new System.EventHandler(this.TrialBalanceForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer tbrViewer;
    }
}