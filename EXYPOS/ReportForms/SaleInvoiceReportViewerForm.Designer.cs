﻿namespace EXYPOS.ReportForms
{
    partial class SaleInvoiceReportViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaleInvoiceReportViewerForm));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.saleInvRepoView = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.loaderPicBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.saleInvRepoView);
            this.groupBox2.Location = new System.Drawing.Point(5, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(383, 482);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Report Preview";
            // 
            // saleInvRepoView
            // 
            this.saleInvRepoView.ActiveViewIndex = -1;
            this.saleInvRepoView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.saleInvRepoView.Cursor = System.Windows.Forms.Cursors.Default;
            this.saleInvRepoView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saleInvRepoView.Location = new System.Drawing.Point(3, 16);
            this.saleInvRepoView.Name = "saleInvRepoView";
            this.saleInvRepoView.ShowCloseButton = false;
            this.saleInvRepoView.ShowGotoPageButton = false;
            this.saleInvRepoView.ShowGroupTreeButton = false;
            this.saleInvRepoView.ShowLogo = false;
            this.saleInvRepoView.ShowPageNavigateButtons = false;
            this.saleInvRepoView.ShowParameterPanelButton = false;
            this.saleInvRepoView.ShowRefreshButton = false;
            this.saleInvRepoView.ShowTextSearchButton = false;
            this.saleInvRepoView.Size = new System.Drawing.Size(377, 463);
            this.saleInvRepoView.TabIndex = 0;
            this.saleInvRepoView.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // loaderPicBox
            // 
            this.loaderPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loaderPicBox.Image = ((System.Drawing.Image)(resources.GetObject("loaderPicBox.Image")));
            this.loaderPicBox.Location = new System.Drawing.Point(315, 3);
            this.loaderPicBox.Name = "loaderPicBox";
            this.loaderPicBox.Size = new System.Drawing.Size(80, 58);
            this.loaderPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loaderPicBox.TabIndex = 17;
            this.loaderPicBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(112, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 23);
            this.label1.TabIndex = 18;
            this.label1.Text = "Sale Invoice";
            // 
            // SaleInvoiceReportViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 568);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loaderPicBox);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaleInvoiceReportViewerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sale Invoice Form";
            this.Load += new System.EventHandler(this.SaleInvoiceReportViewerForm_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer saleInvRepoView;
        private System.Windows.Forms.PictureBox loaderPicBox;
        private System.Windows.Forms.Label label1;
    }
}