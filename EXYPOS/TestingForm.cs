﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS
{
    public partial class TestingForm : Form
    {
        public TestingForm()
        {
            InitializeComponent();
            GroupBox gbox = new GroupBox();
            gbox.Location = new Point(179, 145);
            gbox.Text = "Select Gender";
            gbox.Name = "Mybox";
            gbox.Font = new Font("Colonna MT", 12);
            gbox.Visible = true;
            gbox.AutoSize = true;
            gbox.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            // Adding groupbox in the form 
            this.Controls.Add(gbox);

            // Creating and setting  
            // properties of the CheckBox 
            CheckBox c1 = new CheckBox();
            c1.Location = new Point(40, 42);
            c1.Size = new Size(69, 20);
            c1.Text = "Male";

            // Adding this control  
            // to the GroupBox 
            gbox.Controls.Add(c1);

            // Creating and setting  
            // properties of the CheckBox 
            CheckBox c2 = new CheckBox();
            c2.Location = new Point(183, 39);
            c2.Size = new Size(79, 20);
            c2.Text = "Female";

            // Adding this control 
            // to the GroupBox 
            gbox.Controls.Add(c2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int x = 820+20, y = 16;
            for (int i = 0; i < 6; i++)
            {
                if (i == 0)
                {
                    Button b = new Button();  //Create button
                    b.Click += new EventHandler(button_Click);
                    b.Location = new Point(x, y);  // Set new location
                    b.Name = "button_" + (i + 1).ToString();  // Set button name
                    b.Text = "Click";
                    b.Size = new Size(110, 42);
                    b.Font = new Font("Minion Pro", 20); // Change Button Font
                    b.Padding = new Padding(0);

                    this.Controls.Add(b);
                }
                else
                {
                    if (i % 2 != 0)
                    {
                        x = x + 170; // create button on right side

                        Button b = new Button();
                        b.Location = new Point(x, y);
                        b.Name = "button_" + (i + 1).ToString();
                        b.Text = "Click";
                        b.Size = new Size(110, 42);
                        b.Font = new Font("Minion Pro", 20);
                        b.Padding = new Padding(0);
                        b.Click += new EventHandler(button_Click);
                        this.Controls.Add(b);
                    }
                    else
                    {

                        x = x - 170;/* create button on bottom of first */
                        y = y + 83;

                        Button b = new Button();
                        b.Location = new Point(x, y);
                        b.Name = "button_" + (i + 1).ToString();
                        b.Text = "Clic";
                        b.Size = new Size(110, 42);
                        b.Font = new Font("Minion Pro", 20);
                        b.Padding = new Padding(0);
                        b.Click += new EventHandler(button_Click);
                        this.Controls.Add(b);
                    }
                }
            }
        }
        protected void button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            MessageBox.Show("yes");
            // identify which button was clicked and perform necessary actions
        }

    }
}
