﻿using DAL;
using EXYPOS.AccountForms;
using EXYPOS.ProductForms;
using EXYPOS.ReportForms;
using EXYPOS.Reports;
using EXYPOS.TransactionForms;
using System;
using System.Data;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using EXYPOS.UserManagement;

namespace EXYPOS
{
    public partial class MainForm : Form
    {
        private int currentIndexValue = 1;
        private int OffsetValue = 0;
        int PageOffset = 0;
        int MaxRecords = 10;
        public MainForm()
        {
            InitializeComponent();
        }

        private void refresfToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void administrationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuAddProduct_Click(object sender, EventArgs e)
        {
            ProductForm ProductFrm = new ProductForm();
            ProductFrm.Show(this);
        }

        private void menuProductsList_Click(object sender, EventArgs e)
        {
            ProductListForm productListFrm = new ProductListForm();
            productListFrm.Show(this);
        }

        private void tBtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tBtnSale_Click(object sender, EventArgs e)
        {
            //SaleInvoiceForm sif = new SaleInvoiceForm();
            SaleInvoicePizza sif = new SaleInvoicePizza();
            sif.Show(this);
        }

        private void tBtnPurchase_Click(object sender, EventArgs e)
        {
            PurchaseInvoiceForm pif = new PurchaseInvoiceForm();
            pif.Show(this);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuAccounts_Click(object sender, EventArgs e)
        {
            AccountListForm aacf = new AccountListForm();
            aacf.Show(this);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            loaderPicBox.Show();
            updateGrid(PageOffset, MaxRecords,txtInvoiceNo.Text);
            recentOrderGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            recentOrderGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }
        public async void updateGrid(int PageOffset, int MaxRecords,string invoiceNo)
        {
            loaderPicBox.Show();
            DataTable recentOrderDt = null;
            this.recentOrderGrid.DataSource = null;
            recentOrderGrid.Rows.Clear();
            recentOrderGrid.Refresh();

            await Task.Run(() =>
            {
                 recentOrderDt = DataAccess.instance.SaleInvoice.GetAll((PageOffset), MaxRecords, invoiceNo == "" ? null : txtInvoiceNo.Text);
               
            });
            if(recentOrderDt.Rows.Count>0)
            lblCount.Text = recentOrderDt != null ? Convert.ToInt32(recentOrderDt.Rows[0]["Count"]).ToString():"0";
            // int tempMaxRecord = 2;
            if (recentOrderDt.Rows.Count > 0)
                if (MaxRecords* currentIndexValue > Convert.ToInt32(recentOrderDt.Rows[0]["Count"]))
            {
                txtPageSize.Text = lblCount.Text;
            }
            else
            {
                txtPageSize.Text = (MaxRecords * currentIndexValue).ToString();
               
            }
            this.recentOrderGrid.DataSource = recentOrderDt;

            recentOrderGrid.Columns["Count"].Visible = false;
            loaderPicBox.Hide();
        }
        public async void SearchFromGrid()
        {
            loaderPicBox.Show();
            DataTable recentOrderDt = null;
            this.recentOrderGrid.DataSource = null;
            recentOrderGrid.Rows.Clear();
            recentOrderGrid.Refresh();

            await Task.Run(() =>
            {
                recentOrderDt = DataAccess.instance.SaleInvoice.GetAll((PageOffset), MaxRecords, txtInvoiceNo.Text == "" ? null: txtInvoiceNo.Text);

            });
            if (recentOrderDt.Rows.Count > 0)
                lblCount.Text = recentOrderDt != null ? Convert.ToInt32(recentOrderDt.Rows[0]["Count"]).ToString() : "0";
            txtPageSize.Text = (MaxRecords).ToString();
            this.recentOrderGrid.DataSource = recentOrderDt;

            recentOrderGrid.Columns["Count"].Visible = false;
            loaderPicBox.Hide();
        }


        private void recentOrderGrid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void recentOrderGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                loaderPicBox.Show();
                int invId = Convert.ToInt32(recentOrderGrid.Rows[e.RowIndex].Cells["Id"].Value);
                DataTable recentOrderDt = DataAccess.instance.SaleInvoice.GetInvById(invId);
                SaleInvoiceSmall sinvs = new SaleInvoiceSmall();
                sinvs.SetDataSource(recentOrderDt);
                saleInvRepoView.ReportSource = sinvs;
                saleInvRepoView.Zoom(1);
                loaderPicBox.Hide();
            }
            catch (Exception ex) {
                loaderPicBox.Hide();
            }
        }

        private void menuTrialBalance_Click(object sender, EventArgs e)
        {
            TrialBalanceForm tbf = new TrialBalanceForm();
            tbf.Show(this);
        }

        private void menuCurrentStock_Click(object sender, EventArgs e)
        {
            CurrentStockForm csf = new CurrentStockForm();
            csf.Show(this);
        }

        private void menuPrintBarcodes_Click(object sender, EventArgs e)
        {
            BarcodePrintingForm bpf = new BarcodePrintingForm();
            bpf.Show(this);
        }

        private void tbtnManageProducts_Click(object sender, EventArgs e)
        {
            ProductListForm productListFrm = new ProductListForm();
            productListFrm.Show(this);
        }

        private void tbtnManageBarcode_Click(object sender, EventArgs e)
        {
            ManageBarcodeForm mbf = new ManageBarcodeForm();
            mbf.Show(this);
        }

        private void menuCreateVoucher_Click(object sender, EventArgs e)
        {
            VoucherForm vf = new VoucherForm();
            vf.Show(this);
        }

        private void menuPurchaseInvoiceList_Click(object sender, EventArgs e)
        {
            PurInvListForm pilF = new PurInvListForm();
            pilF.Show(this);
        }

        private void menuCreatePurchaseInvoice_Click(object sender, EventArgs e)
        {
            PurchaseInvoiceForm piF = new PurchaseInvoiceForm();
            piF.Show(this);
        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuRefreshBtn_Click(object sender, EventArgs e)
        {
          
            updateGrid(PageOffset, MaxRecords, txtInvoiceNo.Text);
        }

        private void productCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductCategory pcf = new ProductCategory();
            pcf.Show(this);
        }

        private void suppliersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SuppliersForm sf = new SuppliersForm();
            sf.Show(this);
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomersForm cf = new CustomersForm();
            cf.Show();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtPageSize.Text) >= Convert.ToInt32(lblCount.Text))
            {
                return;
            }
            OffsetValue += 1;
            if (OffsetValue * 10 >= Convert.ToInt32(lblCount.Text))
            {
                txtPageSize.Text = lblCount.Text;
                return;
            }
            else
            {
                currentIndexValue += 1;
                updateGrid(OffsetValue * 10, 10, txtInvoiceNo.Text);
               
            }
           
        }

        private void Prev_Click(object sender, EventArgs e)
        {
            if(Convert.ToInt32(txtPageSize.Text) <= 10)
            {
                return;
            }
            currentIndexValue = currentIndexValue - 1 <= 0 ? 1 : currentIndexValue - 1;
             OffsetValue -= 1;
            if (OffsetValue * 10 < 0)
            {
                txtPageSize.Text = 10+"";
                return;
            }
            else
                updateGrid(OffsetValue*10,10, txtInvoiceNo.Text);
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                updateGrid(PageOffset, MaxRecords, txtInvoiceNo.Text);
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            LoginForm form = new LoginForm();
            form.Show(this);
            
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {

            UserManagement.ChangePassword form = new UserManagement.ChangePassword();
            form.Show(this);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            //AllReporting reporting = new AllReporting();
            //reporting.Show(this);
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllReporting reporting = new AllReporting();
            reporting.Show(this);
        }

        private void generalLedgerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GeneralLedgerForm Gl = new GeneralLedgerForm();
            Gl.Show(this);
        }

        private void accountReceiveableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountReceiveableForm receiveableForm = new AccountReceiveableForm();
            receiveableForm.Show(this);
        }
    }
}
