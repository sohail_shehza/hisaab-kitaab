﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.UserManagement
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            loaderPicBox.Hide();
            this.TopMost = true;
            lblUserName.Hide();
            lblPassword.Hide();
            lblNewPassword.Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            loaderPicBox.Show();
            bool isValid = true;
            if (txtUserName.Text.Trim() == "")
            {
                isValid = false;
                lblUserName.Show();
            }
            if (txtUserPassword.Text.Trim() == "")
            {
                isValid = false;
                lblPassword.Show();
            }
            if (txtNewPassword.Text.Trim() == "")
            {
                isValid = false;
                lblNewPassword.Show();
            }
            var dt = DataAccess.instance.UserManagement.ChangePassword(txtUserName.Text.Trim(), txtUserPassword.Text.Trim(), txtNewPassword.Text.Trim());
            if (dt.Rows.Count > 0)
            {
                isValid = false;
                loaderPicBox.Hide();
                MessageBox.Show("Password Updated Successfully");
                this.Hide();
            }
            //if (isValid)
            //{
            //    this.Visible = false;

            //    MainForm form = new MainForm();
            //    form.Show(this);

            //}
        }
    }
}
