﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXYPOS.Model
{
    public class SaleInvoiceModel
    {
        public int Id { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime Date { get; set; }
        public long Description { get; set; }
        public string Customer { get; set; }
        public decimal FinalDiscount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public string Title { get; set; }
        public int ProductId { get; set; }
        public decimal Quantitiy { get; set; }
        public decimal SalePrice { get; set; }
        public string MobileNo { get; set; }
    }
}
