﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS
{
    public partial class ProductForm : Form
    {
        private Boolean isEditable;
        private int productId;
        public ProductForm(Boolean isEdit = false,int pid = 0)
        {
            this.isEditable = isEdit;
            this.productId = pid; 
            InitializeComponent();
        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            HideAllLabels();
            loaderPicBox.Show();
            this.TopMost = true;
            DataTable pCatDT = DataAccess.instance.ProductCategories.GetAll();
            categoryCombo.DisplayMember = "Title";
            categoryCombo.ValueMember = "Id";
            categoryCombo.DataSource = pCatDT;
            if (this.isEditable)
            {

                try
                {

                    DataTable pDT = DataAccess.instance.Products.GetProductById(this.productId);
                    this.barcodeTxt.Text = pDT.Rows[0].Field<string>("Barcode");
                    this.titleTxt.Text = pDT.Rows[0].Field<string>("Title");
                    this.unitTxt.Text = pDT.Rows[0].Field<string>("Unit");
                    this.purPriceTxt.Text = pDT.Rows[0].Field<decimal>("PurchasePrice").ToString();
                    this.salePriceTxt.Text = pDT.Rows[0].Field<decimal>("SalePrice").ToString();
                    this.categoryCombo.SelectedValue = Convert.ToInt32(pDT.Rows[0].Field<dynamic>("CatId"));
                }
                catch (Exception ex) { }
                loaderPicBox.Hide();

            }
            if (isEditable == false)
            {

                barcodeTxt.Text = DataAccess.instance.Utility.autoNextNumber("ItemCode").ToString();
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            string barcode = this.barcodeTxt.Text.Trim();
            string title = this.titleTxt.Text.Trim();
            int catId = Convert.ToInt32(this.categoryCombo.SelectedValue);
            string unit = this.unitTxt.Text.Trim();
            decimal purchasePrice = this.purPriceTxt.Text.Trim() != "" ? Convert.ToDecimal(this.purPriceTxt.Text.Trim()) : 0;
            decimal salePrice = this.salePriceTxt.Text.Trim() != "" ? Convert.ToDecimal(this.salePriceTxt.Text.Trim()) : 0;
            bool isValid = true;
            if (barcode == "")
            {
                isValid = false;
                lblBarcode.Show();
            }
            if (title == "")
            {
                isValid = false;
                lblTitle.Show();
            }
            if (catId <= 0)
            {
                isValid = false;
                lblCategory.Show();
            }
            if (unit == "")
            {
                isValid = false;
                lblUnit.Show();
            }
            if (purchasePrice <=0)
            {
                isValid = false;
                lblPurchasePrice.Show();
            }
            if (salePrice <=0)
            {
                isValid = false;
                lblSalePrice.Show();
            }
            if (isValid)
            {

                if (isEditable)
                {
                    bool res = DataAccess.instance.Products.updateProduct(this.productId, barcode, title, catId, unit, purchasePrice, salePrice);
                    if (res)
                    {
                        MessageBox.Show("Record Updated");
                    }
                }
                else
                {
                    bool res = DataAccess.instance.Products.insert(barcode, title, catId, unit, purchasePrice, salePrice);
                    if (res)
                    //if (res != null && res > 0)
                    {
                        MessageBox.Show("Record Added");
                        resetFields();
                    }
                }
            }
        }
        private void HideAllLabels()
        {
            lblBarcode.Hide();
            lblTitle.Hide();
            lblUnit.Hide();
            lblPurchasePrice.Hide();
            lblSalePrice.Hide();
            lblCategory.Hide();
        }
        private void resetFields()
        {
            this.barcodeTxt.Text = "";
            this.titleTxt.Text = "";
            this.unitTxt.Text = "";
            this.purPriceTxt.Text = "";
            this.salePriceTxt.Text = "";
            this.categoryCombo.SelectedValue = 0;
            if (isEditable == false)
            {

                barcodeTxt.Text = DataAccess.instance.Utility.autoNextNumber("ItemCode").ToString();
            }
        }


        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void barcodeTxt_TextChanged(object sender, EventArgs e)
        {
            if (barcodeTxt.Text.Trim() == "")
            {
                lblBarcode.Show();
            }
            else
            {
                lblBarcode.Hide();
            }
        }

        private void titleTxt_TextChanged(object sender, EventArgs e)
        {
            if (titleTxt.Text.Trim() == "")
            {
                lblTitle.Show();
            }
            else
            {
                lblTitle.Hide();
            }
        }

        private void categoryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(this.categoryCombo.SelectedValue) <= 0)
            {
                lblCategory.Show();
            }
            else
            {
                lblCategory.Hide();
            }
        }

        private void unitTxt_TextChanged(object sender, EventArgs e)
        {
            if (unitTxt.Text.Trim() == "")
            {
                lblUnit.Show();
            }
            else
            {
                lblUnit.Hide();
            }
        }

        private void purPriceTxt_TextChanged(object sender, EventArgs e)
        {
            if (purPriceTxt.Text.Trim() == "")
            {
                lblPurchasePrice.Show();
            }
            else
            {
                lblPurchasePrice.Hide();
            }
        }

        private void salePriceTxt_TextChanged(object sender, EventArgs e)
        {
            if (salePriceTxt.Text.Trim() == "")
            {
                lblSalePrice.Show();
            }
            else
            {
                lblSalePrice.Hide();
            }
        }
    }
}
