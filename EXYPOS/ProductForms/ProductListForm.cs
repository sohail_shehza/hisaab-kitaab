﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS
{
    public partial class ProductListForm : Form
    {
        private Boolean searchMode;
        public dynamic refForm;
        private int OffsetValue =0;
        private int ActualValue = 1;
        int PageOffset = 0;
        int MaxRecords = 10;
        public ProductListForm(Boolean sMode = false)
        {
            this.searchMode = sMode;
            InitializeComponent();
        }

        private void ProductListForm_Load(object sender, EventArgs e)
        {
            UpdateGrid(PageOffset, MaxRecords);
            ActualValue += 1;
        }
       // private void updateGrid()
       // {
            //DataTable pDT = DataAccess.instance.Products.GetDetailed(1);
            //this.productsGrid.DataSource = pDT;
            //this.productsGrid.Columns["Id"].Visible = false;
            //this.productsGrid.Columns["ImagePath"].Visible = false;
            //this.productsGrid.Columns["CategoryId"].Visible = false;
            //this.productsGrid.Columns["editBtn"].DisplayIndex = Convert.ToInt32(this.productsGrid.ColumnCount) - 1;
            //this.productsGrid.Columns["deleteBtn"].DisplayIndex = Convert.ToInt32(this.productsGrid.ColumnCount) - 1;
            //if(searchMode)
            //{
            //    this.productsGrid.Columns["editBtn"].Visible = false;
            //    this.productsGrid.Columns["deleteBtn"].Visible = false;
            //}
          // UpdateGrid (int PageOffset, int MaxRecords)
           // loaderPicBox.Hide();
       // }

        private void productCodeTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void productCodeTxt_KeyUp(object sender, KeyEventArgs e)
        {
            ((DataTable)productsGrid.DataSource).DefaultView.RowFilter = string.Format("Barcode like '%{0}%'", productCodeTxt.Text.Trim());
        }

        private void productNameTxt_KeyUp(object sender, KeyEventArgs e)
        {
            ((DataTable)productsGrid.DataSource).DefaultView.RowFilter = string.Format("Title like '%{0}%'", productNameTxt.Text.Trim());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void productsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = this.productsGrid.Columns[e.ColumnIndex].Name;
            switch(colName)
            {
                case "editBtn":
                    int pid = Convert.ToInt32(this.productsGrid.Rows[e.RowIndex].Cells["Id"].Value);
                    ProductForm pf = new ProductForm(true,pid);
                    pf.Show(this);
                    break;
                case "deleteBtn":
                    MessageBox.Show("delete clicked");
                    break;
            }
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            loaderPicBox.Show();
            UpdateGrid(1,10);
            ActualValue = 2;
        }


        private void productsGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (searchMode)
            {
                int pId = Convert.ToInt32(this.productsGrid.Rows[e.RowIndex].Cells["Barcode"].Value);
                //refGridView.Rows.Add(pId.ToString());
                refForm.handleSearch(this.productsGrid.Rows[e.RowIndex]);
                this.Close();
            }
        }

       

        private void productsGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.searchMode && e.KeyCode == Keys.Enter)
            {
                foreach (DataGridViewRow row in productsGrid.SelectedRows)
                {
                    refForm.handleSearch(row);
                }
                this.Close();
            }
        }

        private void newProdBtn_Click(object sender, EventArgs e)
        {
            ProductForm pf = new ProductForm();
            pf.Show();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            OffsetValue += 1;
            int totalValue = OffsetValue * 10;
              
            if (ActualValue * 10 >= Convert.ToInt32(lblCount.Text))
            {
                txtPageSize.Text = lblCount.Text;
                ActualValue += 1;
                return;
            }
            else
            {
                UpdateGrid(totalValue, 10);
                txtPageSize.Text = totalValue.ToString();
                ActualValue += 1;
            }

        }

        private void Prev_Click(object sender, EventArgs e)
        {
            int current = OffsetValue;
            OffsetValue= OffsetValue == 0 ? 0: current-1;
            int totalValue = OffsetValue * 10;
            if (ActualValue * 10 <= 0)
            {
                txtPageSize.Text = 10 + "";
                ActualValue -= 1;
                return;
            }
            else
            {
                UpdateGrid(totalValue, 10);
                txtPageSize.Text = totalValue.ToString();
                ActualValue -= 1;
            }
        }
        private async void UpdateGrid(int PageOffset, int MaxRecords)
        {
            loaderPicBox.Show();
            DataTable recentOrderDt = null;
            this.productsGrid.DataSource = null;
            productsGrid.Rows.Clear();
            productsGrid.Refresh();

            await Task.Run(() =>
            {
                recentOrderDt = DataAccess.instance.Products.GetAllProductByCodeAndName((PageOffset), MaxRecords, productCodeTxt.Text == "" ? null : productCodeTxt.Text, productNameTxt.Text == "" ? null : productNameTxt.Text);

            });
            if (recentOrderDt.Rows.Count > 0)
                lblCount.Text = recentOrderDt != null ? Convert.ToInt32(recentOrderDt.Rows[0]["TotalCount"]).ToString() : "0";
            txtPageSize.Text = (MaxRecords).ToString();
            this.productsGrid.DataSource = recentOrderDt;
           // this.productsGrid.DataSource = pDT;
            this.productsGrid.Columns["Id"].Visible = false;
            this.productsGrid.Columns["ImagePath"].Visible = false;
            this.productsGrid.Columns["CategoryId"].Visible = false;
            this.productsGrid.Columns["editBtn"].DisplayIndex = Convert.ToInt32(this.productsGrid.ColumnCount) - 1;
            this.productsGrid.Columns["deleteBtn"].DisplayIndex = Convert.ToInt32(this.productsGrid.ColumnCount) - 1;
            if (searchMode)
            {
                this.productsGrid.Columns["editBtn"].Visible = false;
                this.productsGrid.Columns["deleteBtn"].Visible = false;
            }

            productsGrid.Columns["TotalCount"].Visible = false;
            loaderPicBox.Hide();
        }
    }
}
