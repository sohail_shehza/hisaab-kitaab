﻿using DAL;
using EXYPOS.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.ProductForms
{
    public partial class ManageBarcodeForm : Form
    {
        public ManageBarcodeForm()
        {
            InitializeComponent();
        }

        private void searchProdBtn_Click(object sender, EventArgs e)
        {
            ProductListForm pf = new ProductListForm(true);
            pf.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            pf.refForm = this;
            pf.ShowDialog(this);
        }
        public void handleSearch(DataGridViewRow row)
        {
            try
            {
                loaderPicBox.Show();
                invoiceGrid.Rows.Add(row.Cells["Id"].Value, row.Cells["Barcode"].Value, row.Cells["Title"].Value,  row.Cells["SalePrice"].Value, 1);
                invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["copies"];
                loaderPicBox.Hide();
            }
            catch (Exception ex) { }
            
        }

        private void invoiceGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            string colName = this.invoiceGrid.Columns[e.ColumnIndex].Name;
            switch (colName)
            {
                case "print":
                    int pId = Convert.ToInt32(invoiceGrid.Rows[e.RowIndex].Cells["pId"].Value);
                    DataTable pDT = DataAccess.instance.Products.GetProductById(pId);
                    BarcodeSinglePrint bsp = new BarcodeSinglePrint();
                    bsp.SetDataSource(pDT);
                    reportViewer.ReportSource = bsp;
                    break;
                case "delete":
                    this.invoiceGrid.Rows.RemoveAt(e.RowIndex);
                    break;
            }
        }

        private void prodCodeSrTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                loaderPicBox.Show();
                string itemCode = prodCodeSrTxt.Text;
                DataTable itemDT = DataAccess.instance.Products.GetByItemCode(itemCode);
                if (itemDT.Rows.Count > 0)
                {
                    int Id = itemDT.Rows[0].Field<int>("Id");
                    string Barcode = itemDT.Rows[0].Field<String>("Barcode");
                    String Title = itemDT.Rows[0].Field<String>("Title");
                    String Unit = itemDT.Rows[0].Field<String>("Unit");
                    decimal SalePrice = itemDT.Rows[0].Field<decimal>("SalePrice");
                    int stockAvail = itemDT.Rows[0].Field<int>("StockAvailable");
                    invoiceGrid.Rows.Add(Id, Barcode, Title, SalePrice, 1);
                    invoiceGrid.CurrentCell = invoiceGrid.Rows[invoiceGrid.RowCount - 1].Cells["copies"];
                    loaderPicBox.Hide();
                }
            }
        }

        private void ManageBarcodeForm_Load(object sender, EventArgs e)
        {

        }

        private void printAllBtn_Click(object sender, EventArgs e)
        {
            loaderPicBox.Hide();
        }
    }
}
