﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.AccountForms
{
    public partial class CustomersForm : Form
    {
        public CustomersForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           
            loaderPicBox.Show();
            bool isValid = true;
            if (txtCustomerName.Text.Trim() == "")
            {
                isValid = false;
                lblTitle.Show();
            }
            if (txtCustomerPhone.Text.Trim() == "")
            {
                isValid = false;
                lblPhone.Show();
            }
            if (txtCustomerCountry.Text.Trim() == "")
            {
                isValid = false;
                lblCountry.Show();
            }
            if (txtCustomerCity.Text.Trim() == "")
            {
                isValid = false;
                lblCity.Show();
            }
            if (txtCustomerAddress.Text.Trim() == "")
            {
                isValid = false;
                lblAddress.Show();
            }
            if (isValid)
            {
                int res = DataAccess.instance.Accounts.insert("", txtCustomerName.Text, 1, 2);
                if (res > 0)
                {
                    MessageBox.Show("Record Added");
                    ClearForm();
                    loaderPicBox.Hide();
                }
            }
            loaderPicBox.Hide();
        }
        private void ClearForm()
        {
            txtCustomerName.Text = "";
            txtCustomerAddress.Text = "";
            txtCustomerCity.Text = "";
            txtCustomerCountry.Text = "";
            txtCustomerPhone.Text = "";
        }
        private void HideLabels()
        {
            lblTitle.Hide();
            lblAddress.Hide();
            lblPhone.Hide();
            lblCountry.Hide();
            lblCity.Hide();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void CustomersForm_Load(object sender, EventArgs e)
        {
            loaderPicBox.Hide();
            this.TopMost = true;
            HideLabels();
        }

        private void txtCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerName.Text.Trim() == "")
            {
                lblTitle.Show();
            }
            else
            {
                lblTitle.Hide();
            }
        }

        private void txtCustomerPhone_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerPhone.Text.Trim() == "")
            {
                lblPhone.Show();
            }
            else
            {
                lblPhone.Hide();
            }
        }

        private void txtCustomerCity_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerCity.Text.Trim() == "")
            {
                lblCity.Show();
            }
            else
            {
                lblCity.Hide();
            }
        }

        private void txtCustomerCountry_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerCountry.Text.Trim() == "")
            {
                lblCountry.Show();
            }
            else
            {
                lblCountry.Hide();
            }
        }

        private void txtCustomerAddress_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerAddress.Text.Trim() == "")
            {
                lblAddress.Show();
            }
            else
            {
                lblAddress.Hide();
            }
        }
    }
}
