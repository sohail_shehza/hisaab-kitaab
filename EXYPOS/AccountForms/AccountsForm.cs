﻿using DAL;
using System;
using System.Data;
using System.Windows.Forms;

namespace EXYPOS.AccountForms
{
    public partial class AccountsForm : Form
    {
        private Boolean isEditable;
        private int accountId;
        public AccountsForm(Boolean isEdit = false,int accId=0)
        {
            this.isEditable = isEdit;
            this.accountId = accId;
            InitializeComponent();
        }

        private void AccountsForm_Load(object sender, EventArgs e)
        {
            HideLabel();
            loaderPicBox.Show();
            DataTable accTypeDT = DataAccess.instance.AccountTypes.GetAll();
            typeCombo.DisplayMember = "Type";
            typeCombo.ValueMember = "Id";
            typeCombo.DataSource = accTypeDT;

            DataTable accCatDT = DataAccess.instance.AccountCategories.GetAll();
            categoryCombo.DisplayMember = "Category";
            categoryCombo.ValueMember = "Id";
            categoryCombo.DataSource = accCatDT;

            if (isEditable)
            {
                DataTable accDT = DataAccess.instance.Accounts.GetAccById(this.accountId);
                this.accCodeTxt.Text = accDT.Rows[0].Field<string>("Code");
                this.titleTxt.Text = accDT.Rows[0].Field<string>("Title");
                this.typeCombo.SelectedValue = accDT.Rows[0].Field<Int32>("TypeId");
                this.categoryCombo.SelectedValue = accDT.Rows[0].Field<Int32>("CatId");
            }
            loaderPicBox.Hide();
            //if(isEditable == false)
            //{
            //    accCodeTxt.Text = DataAccess.instance.Utility.autoNextNumber("AccCode").ToString();
            //}
        }


        private void saveBtn_Click(object sender, EventArgs e)
        {
            loaderPicBox.Show();

            string accCode = this.accCodeTxt.Text.Trim();
            string title = this.titleTxt.Text.Trim();
            int typeId = Convert.ToInt32(this.typeCombo.SelectedValue);
            int catId = Convert.ToInt32(this.categoryCombo.SelectedValue);
            bool isValid = true;
            if (accCode == "")
            {
                isValid = false;
                lblAccCode.Show();
            }
            if (title == "")
            {
                isValid = false;
                lblTitle.Show();
            }
            if (typeId <= 0)
            {
                isValid = false;
                lblCategory.Show();
            }
            if (catId<0)
            {
                isValid = false;
                lblType.Show();
            }
            if (isValid)
            {
                if (this.isEditable)
                {
                    int res = DataAccess.instance.Accounts.updateAccount(this.accountId, accCode, title, typeId, catId);
                    if (res > 0)
                    {
                        MessageBox.Show("Account updated Successfully");
                    }
                }
                else
                {
                    int res = DataAccess.instance.Accounts.insert(accCode, title, typeId, catId);
                    if (res > 0)
                    {
                        resetFields();
                        MessageBox.Show("New Account Added Successfully");
                    }
                }
            }
            loaderPicBox.Hide();
        }
        private void resetFields()
        {
            this.accCodeTxt.Text = "";
            this.titleTxt.Text="";
            this.typeCombo.SelectedValue=0;
            this.categoryCombo.SelectedValue=0;
            //if (isEditable == false)
            //{
            //    accCodeTxt.Text = DataAccess.instance.Utility.autoNextNumber("AccCode").ToString();
            //}
        }
        private void HideLabel()
        {
            lblAccCode.Hide();
            lblTitle.Hide();
            lblCategory.Hide();
            lblType.Hide();
        }

        private void accCodeTxt_TextChanged(object sender, EventArgs e)
        {
            if (accCodeTxt.Text.Trim() == "")
            {
                lblAccCode.Show();
            }
            else
            {
                lblAccCode.Hide();
            }
        }

        private void titleTxt_TextChanged(object sender, EventArgs e)
        {
            if (titleTxt.Text.Trim() == "")
            {
                lblTitle.Show();
            }
            else
            {
                lblTitle.Hide();
            }
        }

        private void categoryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int typeId = Convert.ToInt32(this.typeCombo.SelectedValue);
            if (typeId <= 0)
            {
                lblCategory.Show();
            }
            else
            {
                lblCategory.Hide();
            }
        }

        private void typeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int catId = Convert.ToInt32(this.categoryCombo.SelectedValue);
            if (catId <= 0)
            {
                lblCategory.Show();
            }
            else
            {
                lblCategory.Hide();
            }
        }
    }
}
