﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.AccountForms
{
    public partial class SuppliersForm : Form
    {
        public SuppliersForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loaderPicBox.Show();
            bool isValid = true;
            if (txtSupplierName.Text.Trim() == "")
            {
                isValid = false;
                lblTitle.Show();
            }
            if (txtSupplierPhone.Text.Trim() == "")
            {
                isValid = false;
                lblPhone.Show();
            }
            if (txtSupplierCountry.Text.Trim() == "")
            {
                isValid = false;
                lblCountry.Show();
            }
            if (txtSupplierCity.Text.Trim() == "")
            {
                isValid = false;
                lblCity.Show();
            }
            if (txtSupplierAddress.Text.Trim() == "")
            {
                isValid = false;
                lblAddress.Show();
            }
            if (isValid)
            {
                int res = DataAccess.instance.Accounts.insert("", txtSupplierName.Text, 4, 1);
                if (res > 0)
                {
                    MessageBox.Show("Record Added");

                    ClearForm();
                    
                }
            }
            loaderPicBox.Hide();
        }
        private void ClearForm()
        {
            txtSupplierName.Text = "";
            txtSupplierAddress.Text = "";
            txtSupplierCity.Text = "";
            txtSupplierCountry.Text = "";
            txtSupplierPhone.Text = "";
        }
        private void HideLabels()
        {
            lblTitle.Hide();
            lblAddress.Hide();
            lblPhone.Hide();
            lblCountry.Hide();
            lblCity.Hide();
        }
        private void SuppliersForm_Load(object sender, EventArgs e)
        {
            loaderPicBox.Hide();
            HideLabels();
        }

        private void txtSupplierName_TextChanged(object sender, EventArgs e)
        {
            if (txtSupplierName.Text.Trim() == "")
            {
                lblTitle.Show();
            }
            else
            {
                lblTitle.Hide();
            }
        }

        private void txtSupplierPhone_TextChanged(object sender, EventArgs e)
        {
            if (txtSupplierPhone.Text.Trim() == "")
            {
                lblPhone.Show();
            }
            else
            {
                lblPhone.Hide();
            }
        }

        private void txtSupplierCity_TextChanged(object sender, EventArgs e)
        {
            if (txtSupplierCity.Text.Trim() == "")
            {
                lblCity.Show();
            }
            else
            {
                lblCity.Hide();
            }
        }

        private void txtSupplierCountry_TextChanged(object sender, EventArgs e)
        {
            if (txtSupplierCountry.Text.Trim() == "")
            {
                lblCountry.Show();
            }
            else
            {
                lblCountry.Hide();
            }
        }

        private void txtSupplierAddress_TextChanged(object sender, EventArgs e)
        {
            if (txtSupplierAddress.Text.Trim() == "")
            {
                lblAddress.Show();
            }
            else
            {
                lblAddress.Hide();
            }
        }
    }
}
