﻿namespace EXYPOS.AccountForms
{
    partial class AccountReceiveableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountReceiveableForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.txtReceived = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.txtSaleAmount = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.customerCombo = new System.Windows.Forms.ComboBox();
            this.lblNoDuePaymentFound = new System.Windows.Forms.Label();
            this.loaderPicBox = new System.Windows.Forms.PictureBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.invoiceGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Customer";
            // 
            // txtReceived
            // 
            this.txtReceived.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReceived.BackColor = System.Drawing.SystemColors.Window;
            this.txtReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceived.Location = new System.Drawing.Point(406, 54);
            this.txtReceived.Name = "txtReceived";
            this.txtReceived.Size = new System.Drawing.Size(167, 29);
            this.txtReceived.TabIndex = 58;
            this.txtReceived.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtReceived.TextChanged += new System.EventHandler(this.txtReceived_TextChanged);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(337, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 57;
            this.label15.Text = "Received";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(337, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 53;
            this.label11.Text = "Due Amount";
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDueAmount.BackColor = System.Drawing.SystemColors.Control;
            this.txtDueAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDueAmount.Location = new System.Drawing.Point(406, 113);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            this.txtDueAmount.Size = new System.Drawing.Size(167, 29);
            this.txtDueAmount.TabIndex = 54;
            this.txtDueAmount.Text = "0";
            this.txtDueAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSaleAmount
            // 
            this.txtSaleAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtSaleAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtSaleAmount.Enabled = false;
            this.txtSaleAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaleAmount.Location = new System.Drawing.Point(127, 113);
            this.txtSaleAmount.Name = "txtSaleAmount";
            this.txtSaleAmount.ReadOnly = true;
            this.txtSaleAmount.Size = new System.Drawing.Size(178, 29);
            this.txtSaleAmount.TabIndex = 56;
            this.txtSaleAmount.Text = "0";
            this.txtSaleAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(31, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 55;
            this.label13.Text = "Sale Amount";
            // 
            // customerCombo
            // 
            this.customerCombo.FormattingEnabled = true;
            this.customerCombo.Location = new System.Drawing.Point(132, 62);
            this.customerCombo.Name = "customerCombo";
            this.customerCombo.Size = new System.Drawing.Size(178, 21);
            this.customerCombo.TabIndex = 52;
            this.customerCombo.SelectedIndexChanged += new System.EventHandler(this.customerCombo_SelectedIndexChanged);
            // 
            // lblNoDuePaymentFound
            // 
            this.lblNoDuePaymentFound.AutoSize = true;
            this.lblNoDuePaymentFound.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDuePaymentFound.ForeColor = System.Drawing.Color.Red;
            this.lblNoDuePaymentFound.Location = new System.Drawing.Point(197, 9);
            this.lblNoDuePaymentFound.Name = "lblNoDuePaymentFound";
            this.lblNoDuePaymentFound.Size = new System.Drawing.Size(223, 24);
            this.lblNoDuePaymentFound.TabIndex = 60;
            this.lblNoDuePaymentFound.Text = "No due payment found";
            // 
            // loaderPicBox
            // 
            this.loaderPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loaderPicBox.Image = ((System.Drawing.Image)(resources.GetObject("loaderPicBox.Image")));
            this.loaderPicBox.Location = new System.Drawing.Point(522, 3);
            this.loaderPicBox.Name = "loaderPicBox";
            this.loaderPicBox.Size = new System.Drawing.Size(65, 48);
            this.loaderPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loaderPicBox.TabIndex = 61;
            this.loaderPicBox.TabStop = false;
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(489, 160);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(84, 41);
            this.saveBtn.TabIndex = 62;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // invoiceGrid
            // 
            this.invoiceGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.invoiceGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.invoiceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invoiceGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.invoiceGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.invoiceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.invoiceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.invoiceGrid.EnableHeadersVisualStyles = false;
            this.invoiceGrid.Location = new System.Drawing.Point(39, 207);
            this.invoiceGrid.MultiSelect = false;
            this.invoiceGrid.Name = "invoiceGrid";
            this.invoiceGrid.RowHeadersVisible = false;
            this.invoiceGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.invoiceGrid.Size = new System.Drawing.Size(534, 339);
            this.invoiceGrid.TabIndex = 63;
            // 
            // AccountReceiveableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 558);
            this.Controls.Add(this.invoiceGrid);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.loaderPicBox);
            this.Controls.Add(this.lblNoDuePaymentFound);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtReceived);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtDueAmount);
            this.Controls.Add(this.txtSaleAmount);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.customerCombo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccountReceiveableForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Receiveable Form";
            this.Load += new System.EventHandler(this.AccountReceiveableForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.loaderPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtReceived;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDueAmount;
        private System.Windows.Forms.TextBox txtSaleAmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox customerCombo;
        private System.Windows.Forms.Label lblNoDuePaymentFound;
        private System.Windows.Forms.PictureBox loaderPicBox;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.DataGridView invoiceGrid;
    }
}