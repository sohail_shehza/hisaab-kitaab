﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.AccountForms
{
    public partial class AccountReceiveableForm : Form
    {
        private DataTable dt = new DataTable();
        public AccountReceiveableForm()
        {
            InitializeComponent();
        }

        private void AccountReceiveableForm_Load(object sender, EventArgs e)
        {
            lblNoDuePaymentFound.Hide();
            GetCustomerAccount();
            GetAllBadDebts();
        }
        private async void GetCustomerAccount()
        {
            loaderPicBox.Show();
            DataTable csDT = null;
            await Task.Run(() =>
            {
                csDT = DataAccess.instance.Accounts.GetCustomerAccounts();
                //DataRow dr;
                //dr = csDT.NewRow();
                //dr.ItemArray = new object[] { "--Show Customer--" };
                //csDT.Rows.InsertAt(dr, 0);
            });


            customerCombo.DisplayMember = "Title";
            customerCombo.ValueMember = "Id";
            customerCombo.DataSource = csDT;
            customerCombo.SelectedValue = 3;
            loaderPicBox.Hide();
        }

        private void customerCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetDataByCustomerId();
        }

        private void txtReceived_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtReceived.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                txtReceived.Text = txtReceived.Text.Remove(txtReceived.Text.Length - 1);
            }
            else
            {
                if (!string.IsNullOrEmpty(txtReceived.Text))
                {
                    //MessageBox.Show("yes");
                    int receivedAmount = Convert.ToInt32(txtReceived.Text);

                    if (!string.IsNullOrEmpty(txtDueAmount.Text) && receivedAmount > 0)
                    {
                        int dueAmount = Convert.ToInt32(txtDueAmount.Text);
                        if (dueAmount < 0)
                        {
                            dueAmount = dueAmount + receivedAmount;
                            if (dueAmount <= 0)
                            {
                                txtDueAmount.Text = dueAmount.ToString();
                            }
                            else
                            {
                                txtDueAmount.Text = "0";
                            }
                        }

                    }
                }
                else
                {
                    txtDueAmount.Text = (dt.Rows[0].Field<decimal>("BadDebtsAmount")).ToString();
                }
            }
        }
        private void GetDataByCustomerId()
        {
            dt = DataAccess.instance.BadDebts.GetCustomerId(Convert.ToInt32(customerCombo.SelectedValue));
            if (dt.Rows.Count > 0)
            {
                //var CustomerId = dt.Rows[0].Field<Int32>("CustomerId");
                // var MobileNo = dt.Rows[0].Field<String>("MobileNo");
                txtSaleAmount.Text = (dt.Rows[0].Field<decimal>("SaleAmount")).ToString();
                txtDueAmount.Text = (dt.Rows[0].Field<decimal>("BadDebtsAmount")).ToString();
                lblNoDuePaymentFound.Hide();
            }
            else
            {
                txtReceived.Text = "0";
                txtSaleAmount.Text = "0";
                txtDueAmount.Text = "0";
                lblNoDuePaymentFound.Show();
            }
        }
        private void GetAllBadDebts()
        {
           var allrecords = DataAccess.instance.BadDebts.GetAllBadDebts();
            invoiceGrid.DataSource = allrecords;
            this.invoiceGrid.Columns["Id"].Visible = false;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDueAmount.Text))
            {
                int customerId = Convert.ToInt32(customerCombo.SelectedValue);
                int dueAmount = Convert.ToInt32(txtDueAmount.Text);
                var res = DataAccess.instance.BadDebts.updateBadDebts(customerId,dueAmount);
                GetDataByCustomerId();
                GetAllBadDebts();
                MessageBox.Show("Updated successfully");
            }
        }
    }
}
