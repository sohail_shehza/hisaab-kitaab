﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXYPOS.AccountForms
{
    public partial class AccountListForm : Form
    {
        public AccountListForm()
        {
            InitializeComponent();
        }

        private void AccountsForm_Load(object sender, EventArgs e)
        {
            updateGrid();
        }
        private async void updateGrid()
        {
            loaderPicBox.Show();
            DataTable accounts = null;
            await Task.Run(() =>
            {
                accounts = DataAccess.instance.Accounts.GetAll();
            });
            this.accountsGrid.DataSource = accounts;
            this.accountsGrid.Columns["editBtn"].DisplayIndex = Convert.ToInt32(this.accountsGrid.ColumnCount) - 1;
            this.accountsGrid.Columns["deleteBtn"].DisplayIndex = Convert.ToInt32(this.accountsGrid.ColumnCount) - 1;
            loaderPicBox.Hide();
        }

        private void accCodeTxt_KeyUp(object sender, KeyEventArgs e)
        {
            loaderPicBox.Show();
            ((DataTable)accountsGrid.DataSource).DefaultView.RowFilter = string.Format("Code like '%{0}%'", accCodeTxt.Text.Trim());
            loaderPicBox.Hide();
        }

        private void accTitleTxt_KeyUp(object sender, KeyEventArgs e)
        {
            loaderPicBox.Show();
            ((DataTable)accountsGrid.DataSource).DefaultView.RowFilter = string.Format("Title like '%{0}%'",accTitleTxt.Text.Trim());
            loaderPicBox.Hide();
        }

        private void accountsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = this.accountsGrid.Columns[e.ColumnIndex].Name;
            switch (colName)
            {
                case "editBtn":
                    int accId = Convert.ToInt32(this.accountsGrid.Rows[e.RowIndex].Cells["Id"].Value);
                    AccountsForm pf = new AccountsForm(true, accId);
                    pf.Show(this);
                    break;
                case "deleteBtn":
                    MessageBox.Show("delete clicked");
                    break;
            }
        }

        private void newAccBtn_Click(object sender, EventArgs e)
        {
            AccountsForm af = new AccountsForm();
            af.Show(this);
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            updateGrid();
        }
    }
}
